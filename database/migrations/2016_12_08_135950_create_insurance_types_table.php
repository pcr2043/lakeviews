<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Insurance Types table \n";

        Schema::create('insurance_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('reference')->unique();
            $table->timestamps();
        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
