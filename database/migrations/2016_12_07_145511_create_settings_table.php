<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Settings table \n";

        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slogan')->nullable();
            $table->string('email');
            $table->string('support_email');
            $table->string('no_reply_email');
            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->string('number')->nullable();
            $table->string('postcode')->nullable();
            $table->string('language');
            $table->timestamps();

        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
