<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Pages table\n";


        Schema::create('pages', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('alias')->unique();
            $table->longText('content');
            $table->integer('parent')->nullable()->length(10);
            $table->integer('language_id')->unsigned()->length(10);
            $table->tinyInteger('active')->unsigned()->nullable();
            $table->tinyInteger('menu')->unsigned()->nullable();
            $table->integer('order')->nullable()->length(10);
            $table->timestamps();


            echo "Adding Language Foreign Key \n";
            //FOREIGN KEYS
            $table->foreign('language_id', 'ref_page_language')->references('id')->on('languages')->onDelete('cascade');

        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
