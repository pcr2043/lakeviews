<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Contracts table \n";

        Schema::create('contracts', function (Blueprint $table) {

            $table->increments('id');
            $table->string('apolice_number');
            $table->string('first_name');
            $table->string('last_name');
            $table->tinyInteger('genre');
            $table->date('date_birth');
            $table->string('address');
            $table->string('npa');
            $table->string('city');
            $table->string('country');
            $table->integer('alert_interval_days');
            $table->integer('renew_interval_days');
            $table->date('date_start');
            $table->date('date_end');
            $table->text('description');
            $table->longText('long_description');
            $table->tinyInteger('closed');
            $table->string('payment_type');
            $table->decimal('amount');
            $table->integer('customer_id')->unsigned()->length(10);
            $table->integer('insurance_id')->unsigned()->length(10);
            $table->string('best_time_to_call');
            $table->timestamps();

            echo "Adding Insurance Type Foreign Key \n";
            //FOREIGN KEYS
            $table->foreign('insurance_id', 'ref_insurance')->references('id')->on('insurances')->onDelete('cascade');

            echo "Adding UserForeign Key \n";
            //FOREIGN KEYS
            $table->foreign('customer_id', 'ref_customer')->references('id')->on('users')->onDelete('cascade');

            echo "***********************\n";

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
