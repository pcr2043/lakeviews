<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Insurance table \n";

        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('reference')->nullable();
            $table->string('urgence_number')->nullable();
            $table->string('urgence_number_ext')->nullable();
            $table->string('color')->nullable();
            $table->integer('insurance_type_id')->unsigned()->length(10);
            $table->timestamps();

            echo "Adding Insurance Type Foreign Key \n";
            //FOREIGN KEYS
            $table->foreign('insurance_type_id', 'ref_insurance_type')->references('id')->on('insurance_types')->onDelete('cascade');

            echo "***********************\n";

        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
