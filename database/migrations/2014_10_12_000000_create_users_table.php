<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Users table \n";


        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('npa')->nullable();
            $table->string('country')->nullable();
            $table->text('notes')->nullable();
            $table->tinyInteger('realocation')->nullable();
            $table->string('relocation_text')->nullable();
            $table->string('relocation_company')->nullable();
            $table->string('relocation_email')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('type');
            $table->tinyInteger('active');
            $table->string('password_forgot')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
