<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Files Table\n";


        Schema::create('files', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('path');
            $table->string('ext');
            $table->tinyInteger('contract_id')->unsigned()->nullable();
            $table->tinyInteger('user_id')->unsigned()->nullable();
            $table->timestamps();

        });

        echo "***********************\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
