<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Create Translations table\n";

        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->integer('language_id')->unsigned()->length(10);
            $table->integer('word_id')->unsigned()->length(10);
            $table->timestamps();

            echo "Adding Language Foreign Key \n";
            //FOREIGN KEYS
            $table->foreign('language_id', 'ref_language')->references('id')->on('languages')->onDelete('cascade');

            echo "Adding Word Foreign Key \n";
            //FOREIGN KEYS
            $table->foreign('word_id', 'ref_Word')->references('id')->on('words')->onDelete('cascade');

            echo "***********************\n";
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
