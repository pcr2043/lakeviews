<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Developer
        $user = new \App\User();
        $user->first_name = 'Patrick';
        $user->last_name = 'Reis';
        $user->email = "patrick.reis.cabral@gmail.com";
        $user->password = Hash::make('Zion2015');
        $user->type = 1;
        $user->active = 1;
        $user->save();

        //Customer
        $user = new \App\User();
        $user->first_name = 'Firstname';
        $user->last_name = 'Lastname';
        $user->email = "customer@lakeviews.ch";
        $user->password = Hash::make('123asd');
        $user->type = 2;
        $user->active = 0;
        $user->save();

        //Admin
        $user = new \App\User();
        $user->first_name = 'Thomas';
        $user->last_name = 'Spycher';
        $user->email = "thomas.spycher@hotmail.com";
        $user->password = Hash::make('Soltero65');
        $user->type = 1;
        $user->active = 2;
        $user->save();
    }
}
