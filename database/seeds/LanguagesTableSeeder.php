<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $language_en = new \App\Language();
        $language_en->name = 'English';
        $language_en->code = 'EN';
        $language_en->default = 1;
        $language_en->active = 1;
        $language_en->save();

        $language_fr = new \App\Language();
        $language_fr->name = 'French';
        $language_fr->code = 'FR';
        $language_fr->active = 1;
        $language_fr->save();
    }
}
