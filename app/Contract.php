<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Contract extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];
    protected $appends = array('name', 'status');

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getStatusAttribute()
    {
        $today = Carbon::now();

        $end = Carbon::parse($this->date_end);


        $renew_max = Carbon::parse($this->date_end);
        $renew_min = Carbon::parse($this->date_end)->addDay(-$this->renew_interval_days);
        $alert_min = Carbon::parse($this->date_end)->addDay(-$this->alert_interval_days);


        $end = Carbon::parse($this->date_end);
        if ($today < $alert_min)
            return "RUNNING";
        else if ($today >= $alert_min && $today < $renew_min)
            return "ALERT";
        else if ($today >= $renew_min && $today <= $renew_max)
            return "RENEW";
        else if ($today > $end)
            return "EXPIRED";
    }

    public function FullPath()
    {

        return $this->getNameAttribute() . '/' . $this->Insurance->name . '/' . $this->Insurance->InsuranceType->name;
    }

    public function Insurance()
    {
        return $this->belongsTo('App\Insurance');
    }

    public function Customer()
    {

        return $this->belongsTo('App\User');
    }

    public function Files()
    {
        return $this->hasMany('App\File');
    }

    public function scopeUser($query, $year)
    {
        $user = Auth::User();

        return $query->with('Insurance')->with('Customer')->with('Files')->where('customer_id', $user->id)
            ->where('apolice_year', $year)->orWhere('apolice_year', 'int')->get();
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == "customer" && $value != '') {

                    $query = $query->with('Insurance')->with('Customer')->whereHas('Customer', function ($q) use ($value) {
                        $q->Where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', '%' . $value . '%');
                    });

                } else if ($key == "name") {

                    $query = $query->with('Insurance')->with('Customer')->where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', '%' . $value . '%');

                } else if ($key == "company") {

                    $query = $query->with('Insurance')->with('Customer')->whereHas('Insurance', function ($q) use ($value) {
                        $q->Where('name', 'LIKE', '%' . $value . '%');
                    });
                } else if ($key == "type") {

                    $query = $query->with('Insurance')->with('Customer')->whereHas('Insurance', function ($q) use ($value) {
                        $q->with('InsuranceType')->whereHas('InsuranceType', function ($q) use ($value) {
                            $q->Where('name', 'LIKE', '%' . $value . '%');
                        });
                    });
                } else if ($value != '') {
                    $query = $query->with('Insurance')->with('Customer')->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->with('Insurance')->with('Customer')->OrderBy($key, $value);

                }
            }
        }

        return new Dataset($query->with('Insurance')->with('Customer'), $skip, $take);

    }

    public function scopeEdit($query, $id)
    {
        if ($id > 0)
            return Contract::find($id);
        else {
            $contract = new Contract();
            $contract->id = -1;
            $contract->apolice_number = '';
            $contract->apolice_year = '';
            $contract->first_name = '';
            $contract->last_name = '';
            $contract->email = '';
            $contract->gender = 1;
            $contract->date_birth = '';
            $contract->address = '';
            $contract->npa = '';
            $contract->city = '';
            $contract->country = '';
            $contract->alert_interval_days = 0;
            $contract->renew_interval_days = 0;
            $contract->date_start = Carbon::Now()->toDateString();
            $contract->date_end = Carbon::Now()->addMonths(6)->toDateString();
            $contract->description = '';
            $contract->long_description = '';
            $contract->closed = 0;
            $contract->managed = 0;
            $contract->payment_type = 1;
            $contract->amount = 0;
            $contract->customer_id = User::Customers()->first()->id;
            $contract->insurance_id = Insurance::all()->first()->id;
            return $contract;
        }

    }
}
