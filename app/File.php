<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class File extends Model
{
    protected $table = 'files';

    protected $guarded =['id'];
    protected $fillable = ['name', 'ext'];

    public function Contract()
    {
        return $this->belongsTo('\App\Contract');
    }

    public function User()
    {
        return $this->belongsTo('\App\User');
    }

    public function scopeMy($query)
    {
        $user = Auth::User();
        return $query->where('user_id', $user->id)->where('type', 1)->get();
    }
}
