<?php

namespace App;

use App\Utils\Utils;
use Illuminate\Database\Eloquent\Model;

class InsuranceType extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];
    protected $utils;

    function __construct( $attributes = array())
    {
        parent::__construct($attributes);


    }



    public function Insurances()
    {
        return $this->hasMany('App\Insurance');
    }

    public function Contracts()
    {
        return $this->hasMany('App\Contract');
    }


    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {

                if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query, $skip, $take);

    }

    public function scopeEdit($query, $id)
    {
        if ($id > 0)
            return InsuranceType::find($id);
        else {
            $insurance_type = new InsuranceType();
            $insurance_type->id = -1;
            $insurance_type->name = '';
            $insurance_type->reference = '';
            $insurance_type->order = InsuranceType::all()->last()->order + 1;
            return $insurance_type;
        }

    }
}
