<?php

namespace App\Pages;

use App\FormResult;
use App\Page;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;

class PagesBL
{

    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function all()
    {
        return Page::Page($this->request);
    }

    public function save()
    {

        return $this->validatePageRequest()->saveDB();
    }

    public function messages()
    {
        return [
            'name.required' => "The name is required",
            'alias.required' => 'The alias is required',

        ];
    }

    public function validatePageRequest()
    {
        $request = $this->request;

        $this->validate($this->request, [
            'name' => [
                'required'
            ],
            'alias' => [
                'required'
            ],

        ], $this->messages());

        //$this->throwValidationException($request, $validator);


        return $this;
    }


    public function saveDB()
    {
        $result = new FormResult();

        try {
            $page = new Page();

            if ($this->request->id > 0)
                $page = Page::find($this->request->id);


            $page->fill($this->request->all());
            $page->save();


            $result->data = $page;

        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result->toJson();

    }

    public function delete($id)
    {

        $result = new FormResult();

        try {

            if ($id > 0)
                $page = Page::find($id);

            $page->delete();


        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result->toJson();
    }


}