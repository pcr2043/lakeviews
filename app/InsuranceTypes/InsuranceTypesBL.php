<?php

namespace App\InsuranceTypes;


use App\FormResult;
use App\InsuranceType;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class InsuranceTypesBL
{

    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function all()
    {

        return InsuranceType::Page($this->request);
    }

    public function save()
    {
        return $this->validateRequest()->saveDB();
    }

    public function validateRequest()
    {

        $this->validate($this->request, [
            'name' => ['required'],
            'reference' => ['required', Rule::unique('insurance_types')->ignore($this->request->id)]
        ], $this->messages());

        return $this;
    }

    public function messages()
    {
        return [
            'name.required' => "The name is required",
            'reference.required' => "The reference is required",
            'value.exists' => 'The reference already exists'
        ];
    }

    public function saveDB()
    {
        $result = new FormResult();

        try {

            if ($this->request->id > 0)
                $insurance_type = InsuranceType::find($this->request->id);
            else
                $insurance_type = new InsuranceType();

            $insurance_type->fill($this->request->all());
            $insurance_type->save();


        } catch (Exception $ex) {

            $result->result = false;
            $result->result = $ex->getMessage();
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();

        try {
            $insurance_type = InsuranceType::find($id);
            if ($insurance_type != null)
                $insurance_type->delete();
        } catch (Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }
        return $result;
    }

}