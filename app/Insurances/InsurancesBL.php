<?php

namespace App\Insurances;


use App\FormResult;
use App\Insurance;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;

class InsurancesBL
{
    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function all()
    {
        return Insurance::Page($this->request);
    }

    public function save()
    {
        return $this->validateRequest()->saveDB();
    }

    public function validateRequest()
    {

        $this->validate($this->request, [
            'name' => ['required']
        ], $this->messages());

        return $this;
    }

    public function messages()
    {
        return [
            'name.required' => "The name is required",
        ];
    }

    public function saveDB()
    {
        $result = new FormResult();

        try {

            if ($this->request->id > 0)
                $insurance = Insurance::find($this->request->id);
            else
                $insurance = new Insurance();

            $insurance->fill($this->request->all());
            $insurance->save();


        } catch (Exception $ex) {

            $result->result = false;
            $result->result = $ex->getMessage();
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();

        try {
            $insurance = Insurance::find($id);
            if ($insurance != null)
                $insurance->delete();
        } catch (Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }
        return $result;
    }


}