<?php

namespace App\Contracts;

use App\Contract;
use App\File;
use App\FormResult;
use App\Validation\ValidateRequestsForApp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class ContractsBL
{

    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function all()
    {

        return Contract::Page($this->request);
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();
    }

    public function validateRequest()
    {

        $this->validate($this->request, [
            'contract.apolice_number' => ['required'],
            'contract.apolice_year' => ['required'],
            'contract.first_name' => ['required'],
            'contract.last_name' => ['required'],
            'contract.date_birth' => ['required'],
            'contract.address' => ['required'],
            'contract.npa' => ['required'],
            'contract.city' => ['required'],
            'contract.country' => ['required'],
            'contract.date_start' => ['required'],
            'contract.date_end' => ['required']

        ], $this->messages());

        return $this;

    }

    public function messages()
    {

        return [];
    }

    public function saveDB()
    {

        $result = new FormResult();
        try {
            if ($this->request->contract["id"] > 0)
                $contract = Contract::find($this->request->contract["id"]);
            else
                $contract = new Contract();

            $contract->fill($this->request->contract);
            $contract->save();
            $result->data = $contract;

            if (Input::get('files') != NULL) {

                $files = Input::get('files');


                foreach ($files as $file_data) {

                    try {
                        $file = new File();
                        $file->fill($file_data);
                        $file->contract_id = $contract->id;
                        $file->user_id = $contract->Customer->id;
                        $file->save();
                        list($type, $file_data["data"]) = explode(';', $file_data["data"]);
                        list(, $file_data["data"]) = explode(',', $file_data["data"]);
                        $data = base64_decode($file_data["data"]);

                        $year = Carbon::createFromDate($contract->start)->year;
                        $server_path = $contract->Customer->name . '/contracts/'
                            . $file->Contract->FullPath() . '/' . $year . '/' . $file->apolice_number . '/';

                        Storage::disk('s3')->makeDirectory($server_path);
                        $server_path = $server_path . $file->name;
                        Storage::disk('s3')->put($server_path, $data);
                        $file->path = $server_path;
                        $file->save();


                    } catch (Exception $ex) {
                        $result->result = false;
                        $result->status = 'An error ocurred when saving documents, please contact you administrator';

                    }
                }

            }

        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();

        try {
            $contract = Contract::find($id);

            foreach ($contract->Files as $file) {

                if (Storage::disk('s3')->has($file->path))
                    Storage::disk('s3')->delete($file->path);
                $file->delete();
            }

            $contract->delete();

        } catch (\Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result;
    }

}