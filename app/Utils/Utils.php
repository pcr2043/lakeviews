<?php
namespace App\Utils;


use App\Language;
use App\Word;
use Illuminate\Support\Facades\Session;

class Utils
{
    public $translations = [];

    public function __construct()
    {
        if (empty(Session::get('default_language'))) {

            $default_language = Language::where('default', '=', 1)->get()->first();
            $translations = Word::TranslationsByLanguage($default_language)->keyBy('reference');
            Session::put('translations', $translations);
            Session::put('default_language', $default_language);
        }

    }

    public function defaultLanguage()
    {
        return Session::get('default_language');
    }

    public function changeLanguage($id)
    {
        $default_language = Language::where('id', '=', $id)->get()->first();
        $translations = Word::TranslationsByLanguage($default_language)->keyBy('reference');
        Session::put('translations', $translations);
        Session::put('default_language', $default_language);
    }

    public function reloadTranslations()
    {
        $default_language = Session::get('default_language');
        $translations = Word::TranslationsByLanguage($default_language)->keyBy('reference');
        Session::put('translations', $translations);
    }

    public function translate($reference)
    {
        $translations = Session::get('translations');
        if (!empty($translations[$reference]))
            return $translations[$reference]['Translations']->first()->value;
        else
            return 'NO TRANSLATION FOR ' . $reference;
    }

    public function languages()
    {
        return Language::Active();
    }


}

?>