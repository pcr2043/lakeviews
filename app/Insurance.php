<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{

    protected $fillable =[];
    protected $guarded=['id'];
    protected $appends = array('type');

    public function getTypeAttribute()
    {
        return $this->InsuranceType->name;
    }
    

    public  function Contracts()
    {
        return $this->hasMany('App\Contract');
    }

    public function InsuranceType()
    {
        return $this->belongsTo('App\InsuranceType');
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {

                if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query->with('InsuranceType'), $skip, $take);

    }

    public function scopeEdit($query, $id)
    {
        if ($id > 0)
            return Insurance::find($id);
        else {
            $insurance = new Insurance();
            $insurance->id = -1;
            $insurance->name = '';
            $insurance->reference = '';
            $insurance->insurance_type_id = InsuranceType::all()->first()->id;
            $insurance->color ="#133555";
            return $insurance;
        }

    }


}
