<?php

namespace App\Message;

use App\FormResult;
use App\Utils\Utils;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MessageBL
{
    use ValidateRequestsForApp;

    protected $request;
    protected $utils;

    public function __construct(Request $request, Utils $utils)
    {
        $this->request = $request;
        $this->utils = $utils;
    }

    public function sendContact()
    {

        return $this->validateSendContactRequest()->sendContactMsg();
    }


    public function messagesContact()
    {
        return [
            'name.required' => $this->utils->translate('name-required'),
            'email.required' => $this->utils->translate('email-required'),
            'availability.required' => $this->utils->translate('availability-required'),
            'message.required' => $this->utils->translate('message-required'),
        ];
    }

    public function validateSendContactRequest()
    {

        $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required',
            'availability' => 'required',
            'message' => 'required'
        ], $this->messagesContact());


        return $this;

    }

    public function sendContactMsg()
    {
        $result = new FormResult();

        try {

            $message = [];
            $message['name'] = $this->request->name;
            $message['from'] = $this->request->email;
            $message['mobile'] = $this->request->mobile;
            $message['availability'] = $this->request->availability;
            $message['to'] = $this->request->email;
            $message['subject'] = 'LAKEVIEWS - MESSAGE CONFIRMATION';
            $message['body'] = $this->request->message;

            $resultSend = Mail::send('messages.contact', ['data' => $message], function ($msg) use ($message) {

                $msg->to($message['to'], 'Lakeviews')->subject($message['subject']);

            });
            $message['subject'] = 'LAKEVIEWS - NEW MESSAGE FROM ' . $this->request->email;
            $message['to'] = "info@lakeviews.ch";
            $resultSend = Mail::send('messages.contact', ['data' => $message], function ($msg) use ($message) {

                $msg->to($message['to'], $message['name'])->subject($message['subject']);

            });

        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
        }


        return $result;
    }

    public function sendRelocationMessage()
    {

        $result = new FormResult();
        try {

            $message = [];
            $message['name'] = $this->request->name;
            $message['from'] = $this->request->from;
            $message['to'] = $this->request->to;
            $message['subject'] = 'RELOCATION MESSAGE FROM ' . $this->request->name;
            $message['body'] = $this->request->message;

            $resultSend = Mail::send('messages.generic', ['data' => $message], function ($msg) use ($message) {

                $msg->to($message['to'], 'Lakeviews')->subject($message['subject']);

            });


        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result;

    }


    public function sendContractMessage()
    {

        $result = new FormResult();
        try {

            $message = [];
            $message['name'] = $this->request->name;
            $message['from'] = $this->request->from;
            $message['to'] = $this->request->to;
            $message['subject'] = 'CONTRACTS MESSAGE FROM ' . $this->request->name;
            $message['body'] = $this->request->message;

            $resultSend = Mail::send('messages.generic', ['data' => $message], function ($msg) use ($message) {

                $msg->to($message['to'], 'Lakeviews')->subject($message['subject']);

            });


        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result;

    }
}