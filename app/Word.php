<?php

namespace App;

use App\Word;
use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    public function Translations()
    {
        return $this->hasMany('App\Translation')->with('Language');
    }

    public function Language()
    {
       return  $this->belongsTo('App\Language');
    }

    public function scopeWithTranslations($query)
    {
        return $query->with('Translations')->get();

    }

    public function scopeTranslationsByLanguage($query, $language)
    {
        return $query->with(array('Translations' => function ($query) use ($language) {
            $query->where('language_id', '=', $language->id);
        }))->get();
    }


    public function scopeEdit($query, $id)
    {
        if($id > 0)
            $word = Word::with('Translations')->find($id); 
        else
        {
            $word = new Word();
            $word->id = -1;
            $word->reference = "";
            $word->value = "";

        }
        
        return $word;
    }

    public function scopePage($query, $request)
    {

        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {

                if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query, $skip, $take);

    }


}
