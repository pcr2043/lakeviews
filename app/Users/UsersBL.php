<?php

namespace App\Users;

use App\Field;
use App\File;
use App\FormResult;
use App\User;
use App\Validation\ValidateRequestsForApp;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UsersBL
{

    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function all()
    {
        return User::Page($this->request);
    }


    public function save()
    {

        return $this->validateRequest()->saveDB();
    }

    public function validateRequest()
    {

        $this->validate($this->request, [
            'user.first_name' => ['required'],
            'user.last_name' => ['required'],
            'user.email' => ['required']

        ], $this->messages());

        return $this;

    }

    public function messages()
    {

        return [
            'user.first_name.required' => "The firstname is required",
            'user.last_name.required' => "The lastname is required",
            'user.email.required' => "The email is required"
        ];
    }

    public function saveDB()
    {

        $result = new FormResult();
        try {
            if ($this->request->user["id"] > 0)
                $user = User::find($this->request->user["id"]);
            else
                $user = new User();

            $user->fill($this->request->user);
            if (!empty($this->request->user['password']) &&  strlen($this->request->user['password']))
                $user->password = Hash::make($this->request->user['password']);

            $user->save();


            if (Input::get('files') != NULL) {

                $files = Input::get('files');


                foreach ($files as $file_data) {

                    try {
                        $file = new File();
                        $file->fill($file_data);
                        //relocation Type
                        $file->type = 2;
                        $file->user_id = $user->id;
                        $file->save();

                        list($type, $file_data["data"]) = explode(';', $file_data["data"]);
                        list(, $file_data["data"]) = explode(',', $file_data["data"]);
                        $data = base64_decode($file_data["data"]);

                        $year = Carbon::Now()->year;
                        $server_path = $file->User->name . '/realocation/' . $year . '/';

                        Storage::disk('s3')->makeDirectory($server_path);
                        $server_path = $server_path . $file->name;
                        Storage::disk('s3')->put($server_path, $data);
                        $file->path = $server_path;
                        $file->save();

                    } catch (Exception $ex) {
                        $result->result = false;
                        $result->status = 'An error ocurred when saving documents, please contact you administrator';

                    }
                }

            }

        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = $ex->getMessage();
            if($ex->getCode() == 23000)
            {
                $result->data ="Model Validation Errors";
                $field = new Field("user.email", false, 'has-error',"Mail already exists");
                $result->addField($field);
                return new JsonResponse($result, 422);
            }
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();

        try {

            $user = User::Find($id);

            if (Storage::disk('s3')->has($user->name))
                Storage::disk('s3')->deleteDirectory($user->name);


            File::where('user_id', $user->id)->delete();
            $user->delete();

        } catch (\Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }

        return $result;
    }


}

?>