<?php

namespace App\Languages;

use App\FormResult;
use App\Language;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Created by PhpStorm.
 * User: etern
 * Date: 09/12/2016
 * Time: 13:28
 */
class LanguagesBL
{
    use ValidateRequestsForApp;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function all()
    {
        return Language::Page($this->request);
    }

    public function save()
    {
        return $this->validateLanguageRequest()->saveDB();
    }

    public function messages()
    {
        return [
            'name.required' => "The name is required",
            'name.unique' => "This name already exists",
            'code.required' => 'The code is required',
            'code.unique' => 'The code already exists'
        ];
    }

    public function validateLanguageRequest()
    {
        $request = $this->request;

        $this->validate($this->request, [
            'name' => [
                'required',
                Rule::unique('languages')->ignore($request->id),
            ],
            'code' => [
                'required',
                Rule::unique('languages')->ignore($request->id),
            ],

        ], $this->messages());

        //$this->throwValidationException($request, $validator);


        return $this;
    }


    public function saveDB()
    {
        $result = new FormResult();

        try {
            $language = new Language();

            if ($this->request->id > 0)
                $language = Language::find($this->request->id);


            //check is default is checked

            if ($this->request->default == 1)
                Language::RemoveDefault();

            $language->fill($this->request->all());
            $language->save();


            $result->data = $language;

        } catch (\Exception $ex) {

            $result->result = false;
            $result->data = "Server error";
        }

        return $result->toJson();

    }

    function remove($id)
    {

        $result = new FormResult();

        try{

            $language = Language::find($id);
            $language->delete();

        }
        catch (\Exception $ex)
        {
            $result->result = false;
            $result->data = $ex->getMessage();


        }

        return $result->toJson();
    }

}