<?php

namespace App;

use App\Utils\Utils;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];

    public function Language()
    {

        return $this->belongsTo('App\Language');
    }

    public function scopeExcept($query, $id)
    {
        return $query->where('id', '!=', $id)->get();
    }

    public function scopeByLanguage($query, $language)
    {
        return $query->where('active', 1)->where('menu', 1)->where('language_id', $language->id)->orderBy('order', 'ASC')->get();

    }

    public function scopeAlias($query, $alias, Utils $utils)
    {
        return $query->where('language_id', $utils->defaultLanguage()->id)->where('alias', $alias)->get()->first();
    }

    public function getPath()
    {

        return $this->RecursiveName($this) . ' <a href="/' . $this->alias . '">' . $this->name . '</a>';
    }

    public function RecursiveName($page)
    {

        if ($page->parent > 0) {
            $parent = Page::find($page->parent);
            $path = $this->RecursiveName($parent) . ' <a href="/' . $parent->alias . '">' . $parent->name . '</a> /';
            return $path;
        } else
            return "";
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {

                if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query->with('language'), $skip, $take);

    }

    public function get_child($page)
    {
        return Page::where('parent', $page->id)->get();
    }

    public function tree($page, $selected, $index)
    {

        $child_pages = $this->get_child($page);

        if (count($child_pages) > 0) {

            if ($index == 0) {
                if ($selected != null && $selected->id == $page->id)
                    echo '<li class="dropdown active">';
                else
                    echo '<li class="dropdown">';

                echo '<a class="dropdown-toggle js-activated" href="/' . $page->alias . '">' . $page->name . '</a> <i  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="fa fa-caret-down"></i>';
                echo '<ul class="dropdown-menu">';

            } else {
                if ($selected != null && $selected->id == $page->id)
                    echo '<li class="dropdown-submenu active">';
                else
                    echo '<li class="dropdown-submenu">';

                echo '<a href="/' . $page->alias . '">' . $page->name . '</span></a>';
                echo '<ul class="dropdown-menu">';


            }

            foreach ($child_pages as $child_page)
                $this->tree($child_page, $selected, ($index + 1));
            echo '</ul>';
            echo '</li>';

        } else {

            if ($selected != null && $selected->id == $page->id)
                echo '<li class="active">';
            else
                echo '<li>';
            echo '<a href="/' . $page->alias . '">' . $page->name . '</a></li>';

        }
    }


    public function scopeEdit($query, $id)
    {
        $page = new Page();
        if ($id > 0)
            $page = Page::find($id);
        else {
            $page->id = '-1';
            $page->name = '';
            $page->alias = '';
            $page->language_id = Language::GetDefault()->id;
            $page->parent = null;
            $page->active = 0;
            $page->menu = 0;
            $page->content = '';
            $page->order = 1;
        }
        return $page;
    }
}
