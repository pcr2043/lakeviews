<?php

namespace App\Words;

use App\FormResult;
use App\Translation;
use App\Utils\Utils;
use App\Validation\ValidateRequestsForApp;
use App\Word;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class WordsBL
{
    use ValidateRequestsForApp;

    protected $request;
    protected $utils;

    public function __construct(Request $request, Utils $utils)
    {
        $this->request = $request;
        $this->utils = $utils;
    }

    public function all()
    {
        return Word::Page($this->request);
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();
    }

    public function messages()
    {

        return [
            'reference.required' => "The reference is required",
            'reference.exists' => "The reference already exists",
            'value.required' => 'The value is required'
        ];
    }

    public function validateRequest()
    {


        $this->validate($this->request, [
            'reference' => [
                'required',
                Rule::unique('words')->ignore($this->request->id),
            ],
            'value' => ['required']
        ], $this->messages());


        return $this;

    }


    public function saveDB()
    {


        $result = new FormResult();

        try {

            if ($this->request->id > 0)
                $word = Word::find($this->request->id);
            else
                $word = new Word();

            $word->reference = $this->request->reference;
            $word->value = $this->request->value;
            $word->save();

            $result->data = $word;

            if (count($this->request->translations > 0)) {
                $word->Translations()->delete();

                foreach ($this->request->translations as $translation_data) {


                    if (strlen($translation_data["value"]) > 0) {
                        $translation = new Translation();
                        $translation->value = $translation_data["value"];
                        $translation->language_id = $translation_data["language_id"];
                        $word->Translations()->save($translation);
                    }

                    $word->save();
                    $this->utils->reloadTranslations();
                }

            }


        } catch (Exception $ex) {

            $result->result = false;
            $result->result = $ex->getMessage();
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try {
            $word = Word::find($id);
            if ($word != null)
                $word->delete();
        } catch (Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }
        return $result;

    }


}