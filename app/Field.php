<?php

namespace App;

class Field
{
	public $name;
	public $class;
	public $msg;
	public $state;

	function __construct($name="", $state= false, $class="", $msg = "")
	{
		$this->name = $name;
		$this->state = $state;
		$this->class = $class;
		$this->msg = $msg;
	}
}


?>