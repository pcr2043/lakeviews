<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable =[];

    public function Language()
    {
        return $this->belongsTo('App\Language');
    }

    public function Word()
    {
        return $this->belongsTo('App\Word');
    }

    public function scopeByLanguage($query, $language)
    {
        return $query->where('language_id','=', $language->id)->get();
    }

}
