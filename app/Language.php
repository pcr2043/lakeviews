<?php

namespace App;

use App\Dataset;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = [];
    protected $guarded = ['id'];

    public function Translations()
    {
        return $this->hasMany('App\Translation');
    }

    public function Words()
    {
        return $this->hasMany('App\Word');
    }

    public function Pages()
    {
        return $this->hasMany('App\Page');
    }

    public function scopeGetDefault($query)
    {
        return $query->where('active', 1)->where('default', 1)->get()->first();
    }

    public function scopeActive($query)
    {
        return $query->where('active', '=', 1)->get();
    }

    public function scopeRemoveDefault($query)
    {
        return $query->where('default', 1)->update(['default' => 0]);
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {

                if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query, $skip, $take);

    }

    public function scopeEdit($query, $id)
    {
        if ($id > 0)
            $language = Language::find($id);
        else {
            $language = new Language();
            $language->id = -1;
            $language->name = "";
            $language->code = "";
            $language->active = 1;
            $language->default = 0;
        }

        return $language;
    }


}
