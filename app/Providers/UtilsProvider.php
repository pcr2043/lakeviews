<?php

namespace App\Providers;

use App\Utils\Utils;
use Illuminate\Support\ServiceProvider;

class UtilsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->share('utils', new Utils());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Utils::class, function ($app) {
            $utils = new Utils();
            return new $utils;
        });
    }
}
