<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'password'];

    protected $fillable = [];

    protected $appends = array('name');

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function Contracts()
    {

        return $this->hasMany('App\Contract');
    }

    public function Files()
    {

        return $this->hasMany('App\File');
    }

    public function RelocationFiles()
    {
        return $this->Files()->where('type', 2)->get();
    }

    public function scopeCustomers($query)
    {
        return $query->where('type', 2)->get();
    }



    public function hasRole($roles)
    {
        foreach ($roles as $role) {

            if ($this->type == $this->ParseRole($role))
                return true;

        }
        return false;
    }

    public function ParseRole($role)
    {
        switch ($role) {

            case 'admin':
                return 1;
                break;

            case  'customer':
                return 2;
                break;

            default:
                return 0;
                break;
        }
    }

    public function isAdmin()
    {
        if ($this->type == 1)
            return true;
        return false;
    }

    public function isCustomer()
    {
        if ($this->type == 2)
            return true;
        return false;
    }

    public function scopePage($query, $request)
    {
        $skip = $request->get("skip");
        $take = $request->get("take");
        $filters = $request->get("filters");

        if (!empty($filters)) {
            foreach ($filters as $key => $value) {


                if ($key == 'name' && $value != '') {
                    $query = $query->where(DB::raw("CONCAT(`first_name`, ' ', `last_name`)"), 'LIKE', '%' . $value . '%');
                } else if ($value != '') {
                    $query = $query->where($key, 'LIKE', '%' . $value . '%');
                }
            }
        }

        $sorts = $request->get("sorts");
        if (!empty($sorts)) {
            foreach ($sorts as $key => $value) {


                if ($value != '') {

                    $query = $query->OrderBy($key, $value);

                }
            }
        }


        return new Dataset($query, $skip, $take);

    }

    public function scopeEdit($query, $id)
    {
        if ($id > 0) {
            $user = User::find($id);
        } else {
            $user = new User();
            $user->id = -1;
            $user->first_name = '';
            $user->last_name = '';
            $user->email = '';
            $user->password = '';
            $user->type = 2;
            $user->active = 1;
            $user->gender = 1;
            $user->phone = '';
            $user->mobile = '';
            $user->address = '';
            $user->city = '';
            $user->npa = '';
            $user->country = '';
            $user->notes = '';
            $user->relocation = 0;
            $user->relocation_text = '';
            $user->relocation_company = '';
            $user->relocation_email = '';
            $user->relocation_address = '';
            $user->relocation_latitude = '';
            $user->relocation_longitude = '';
            $user->date_birth = '';
            $user->best_time_to_call = '';
        }

        return $user;
    }


}
