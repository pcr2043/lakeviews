<?php

namespace App\Settings;

use App\FormResult;
use App\Settings;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class SettingsBO
{
    use ValidateRequestsForApp;


    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();

    }


    public function messages()
    {

        return [
            'name.required' => "Le nom de la categorie es necessaire",
            'slogan.required' => 'Slogan ce un champ necessaire',
            'email.required' => "Email ce un cham necessaire",
            'no_reply_email.required' => "No Reply Email ce un cham necessaire",
            'support_email.required' => "Support Email ce un cham necessaire",
            'country.required' => "Country ce um champ necessaire",
            'address.required' => "Address ce um champ necessaire",
            'number.required' => "Number ce um champ necessaire",
            'postcode.required' => "Address ce um champ necessaire",
            'language.required' => "Language ce um champ necessaire",
            'currency.required' => "Currency ce um champ necessaire",
            'tva.required' => "TVA ace um champ necessaire"
        ];
    }

    public function validateRequest()
    {


        $this->validate($this->request, [
            'name' => 'required',
            'slogan' => 'required',
            'email' => 'required',
            'no_reply_email' => 'required',
            'support_email' => 'required',
            'country' => 'required',
            'address' => 'required',
            'number' => 'required',
            'postcode' => 'required',
            'language' => 'required',
            'currency' => 'required',
            'tva' => 'required'
        ], $this->messages());

        return $this;

    }


    public function saveDB()
    {


        $result = new FormResult();

        try {


            $settings = Settings::all()->first();

            if (empty($settings))
                $settings = new Settings();

            $settings->fill($this->request->all());

            $settings->save();

        } catch (Exception $ex) {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete()
    {
        $result = new FormResult();
        $settings = Settings::all()->first()->delete();

        return $result->toJson();

    }

}

?>