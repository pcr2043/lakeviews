<?php
namespace App\Auth;

use App\FormResult;
use App\Utils\Utils;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class Authentication
{
    use ValidateRequestsForApp;


    protected $request;
    protected $utils;

    public function __construct(Request $request, Utils $utils)
    {
        $this->request = $request;
        $this->utils = $utils;
    }

    public function login()
    {
        return $this->validateLoginRequest()->authenticate();

    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }


    public function messages()
    {
        return [
            'email.required' => $this->utils->translate('email-is-required'),
            'email.exists' => $this->utils->translate('email-not-exists'),
            'password.required' => $this->utils->translate('password-required'),
            'password.min' => $this->utils->translate('password-min'),
        ];
    }

    public function validateLoginRequest()
    {

        $this->validate($this->request, [
            'email' => 'required|email|exists:users',
            'password' => 'required|min:6'
        ], $this->messages());

        //$this->throwValidationException($request, $validator);


        return $this;

    }


    public function authenticate()
    {
        $form = new FormResult(false);

        if (Auth::attempt(['email' => $this->request->email, 'password' => $this->request->password])) {
            // Authentication passed...
            $form->result = true;
            $form->data = Auth::User();
        }

        return new JsonResponse($form);
    }

}

?>