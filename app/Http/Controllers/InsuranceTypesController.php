<?php

namespace App\Http\Controllers;

use App\InsuranceType;
use App\InsuranceTypes\InsuranceTypesBL;
use Illuminate\Http\Request;

class InsuranceTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('insurance-types.index');
    }

    public function all(InsuranceTypesBL $insuranceTypesBL)
    {
        return $insuranceTypesBL->all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, InsuranceTypesBL $insuranceTypesBL)
    {
        return $insuranceTypesBL->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, InsuranceTypesBL $insuranceTypesBL)
    {
        $insurance_type = InsuranceType::Edit($id);
        return view('insurance-types.edit')->with("insurance_type", $insurance_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, InsuranceTypesBL $insuranceTypesBL)
    {
        return $insuranceTypesBL->delete($id)->toJson();
    }
}
