<?php

namespace App\Http\Controllers;

use App\Contract;
use App\File;
use App\FormResult;
use App\InsuranceType;
use App\Utils\Utils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customers.index');
    }

    public function relocation()
    {
        $user = Auth::User();
        return view('customers.relocation')->with("user", $user);
    }

    public function contracts()
    {
        $user = Auth::User();
        return view('customers.contracts')->with("user", $user);
    }

    public function customerContracts(Request $request, Utils $utils)
    {
        $contracts = Contract::User($request->year);
        $insurancesTypes = InsuranceType::orderBy('order', 'asc')->get()->toArray();

        $final = [];
        foreach ($insurancesTypes as $type) {
            $type["translation"] = $utils->translate($type['reference']);
            $type["contracts"] = [];
            $type["all"] = 0;
            $type["monthly"] = 0;
            $type["quarterly"] = 0;
            $type["halfyear"] = 0;
            $type["annual"] = 0;

            foreach ($contracts as $contract) {
                if ($contract->Insurance->InsuranceType->id === $type["id"]) {
                    array_push($type["contracts"], $contract);

                }

            }

            if (count($type["contracts"]) > 0)
                array_push($final, $type);

        }


        return $final;
    }

    public function documents()
    {
        return view('customers.documents');
    }

    public function customerUploads(Request $request)
    {
        $files = File::My();
        return $files;

    }

    public function customerUpload(Request $request)
    {
        $result = new FormResult();

        try {

            $file = new File();
            $file->name = $request->name;
            $file->ext = $request->ext;
            $file->description = $request->description;
            //relocation Type
            $file->type = 1;
            $file->user_id = Auth::User()->id;

            $file->save();

            list($type, $request->data) = explode(';', $request->data);
            list(, $request->data) = explode(',', $request->data);
            $data = base64_decode($request->data);

            $year = Carbon::Now()->year;
            $server_path = $file->User->name . '/documents/' . $year . '/';

            Storage::disk('s3')->makeDirectory($server_path);
            $server_path = $server_path . $file->name;
            Storage::disk('s3')->put($server_path, $data);
            $file->path = $server_path;
            $file->save();

        } catch (\Exception $ex) {
            $result->result = false;
            $result->data = $ex->getMessage();
        }


        return $result->toJson();
    }


}
