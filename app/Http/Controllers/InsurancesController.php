<?php

namespace App\Http\Controllers;


use App\Insurance;
use App\Insurances\InsurancesBL;
use App\InsuranceType;
use Illuminate\Http\Request;

class InsurancesController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insurance_types = InsuranceType::all();
        return view('insurances.index')->with("insurance_types", $insurance_types);
    }

    public function all(InsurancesBL $insurancesBL)
    {
        return $insurancesBL->all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, InsurancesBL $insurancesBL)
    {
            return $insurancesBL->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $insurance = Insurance::Edit($id);
        $insurance_types = InsuranceType::all();

        return view('insurances.edit')->with("insurance", $insurance)->with("insurance_types", $insurance_types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, InsurancesBL $insurancesBL)
    {
        return $insurancesBL->delete($id)->toJson();
    }
}
