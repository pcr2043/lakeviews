<?php

namespace App\Http\Controllers;


use App\Language;
use App\Languages\LanguagesBL;
use App\Utils\Utils;
use Illuminate\Http\Request;

class LanguagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin')->except('change');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('languages.index');
    }


    public function all(LanguagesBL $languagesBL)
    {
        return $languagesBL->all()->ToJson();
    }

    public function change($id, Utils $utils)
    {
        $utils->changeLanguage($id);

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, LanguagesBL $languagesBL)
    {
        return $languagesBL->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $language = Language::Edit($id);
        return view('languages.edit')->with('language', $language);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, LanguagesBL $languagesBL)
    {

        return $languagesBL->remove($id);
    }
}
