<?php

namespace App\Http\Controllers;

use App\Message\MessageBL;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function sendContact(MessageBL $messageBL)
    {
        return $messageBL->sendContact()->toJson();
    }

    public function sendRelocation(MessageBL $messageBL)
    {
        return $messageBL->sendRelocationMessage()->toJson();
    }

    public function sendContracts(MessageBL $messageBL)
    {
        return $messageBL->sendContractMessage()->toJson();
    }
}
