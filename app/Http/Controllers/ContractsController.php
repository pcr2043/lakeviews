<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Contracts\ContractsBL;
use App\Insurance;
use App\User;
use Illuminate\Http\Request;

class ContractsController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contracts.index');
    }


    public function customer()
    {
        return view('contracts.customer');
    }


    public function all(ContractsBL $contractsBL)
    {
        return $contractsBL->all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ContractsBL $contractsBL)
    {
        return $contractsBL->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = Contract::Edit($id);
        $customers = User::all();
        $insuraces = Insurance::with('InsuranceType')->get();
        return view('contracts.edit')->with("contract", $contract)->with("customers", $customers)->with("insurances", $insuraces);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, ContractsBL $contractsBL)
    {
        return $contractsBL->delete($id)->toJson();
    }
}
