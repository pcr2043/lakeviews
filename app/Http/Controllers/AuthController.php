<?php

namespace App\Http\Controllers;

use App\Auth\Authentication;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function authenticate(Authentication $auth)
    {
        return $auth->login();
    }

    public function logout(Authentication $auth)
    {
        return $auth->logout();
    }
}
