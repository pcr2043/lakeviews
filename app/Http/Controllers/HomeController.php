<?php

namespace App\Http\Controllers;

use App\Page;
use App\Utils\Utils;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function admin()
    {
        return view('admin.index');
    }

    public function contacts()
    {
        return view('contacts.index');
    }

    public function alias($alias, Utils $utils)
    {
        $page = Page::Alias($alias, $utils);


        return view('alias')->with("page", $page);
    }
}
