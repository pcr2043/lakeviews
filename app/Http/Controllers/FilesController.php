<?php

namespace App\Http\Controllers;

use App\Contract;
use App\File;
use App\FormResult;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class FilesController extends Controller
{
    public function contract($id)
    {
        return Contract::find($id)->Files()->get();
    }

    public function relocation($id)
    {

        return User::find($id)->RelocationFiles();

    }


    public function download($id)
    {
        $file = File::find($id);
        $permitions = false;

        $user = Auth::user();


        if ($user->isAdmin() || $file->user_id == $user->id || $file->Contract->customer_id == $user->id)
            $permitions = true;


        if ($permitions) {


            $fileContent = Storage::disk('s3')->get($file->path);
            $mimetype = Storage::disk('s3')->mimeType($file->path);
            $size = Storage::disk('s3')->size($file->path);

            $response = response($fileContent, 200, [
                'Content-Type' => $mimetype,
                'Content-Length' => $size,
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename={$file->name}",
                'Content-Transfer-Encoding' => 'binary',
            ]);


            return $response;


        } else
            return redirect()->route('login');
    }

    public function delete($id)
    {
        $file = File::find($id);

        $user = Auth::user();

        if ($user->isAdmin() || $file->user_id == $user->id || $file->Contract->customer_id == $user->id)
            $permitions = true;

        $result = new FormResult();

        if ($permitions) {

            try {
                $file = File::find($id);
                if (Storage::disk('s3')->has($file->path))
                    Storage::disk('s3')->delete($file->path);
                $file->delete();
            } catch (\Exception $ex) {
                $result->result = false;
                $result->data = $ex->getMessage();
            }
        } else {
            $result->result = false;
            $result->data = "no permition";

        }

        return $result->toJson();
    }
}
