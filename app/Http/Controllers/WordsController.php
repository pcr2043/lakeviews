<?php

namespace App\Http\Controllers;

use App\Language;
use App\Translation;
use App\Word;
use App\Words\WordsBL;
use Illuminate\Http\Request;

class WordsController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('words.index');
    }


    public function all(WordsBL $wordsBL)
    {
        return $wordsBL->all()->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WordsBL $wordsBL)
    {
        return $wordsBL->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $word = Word::Edit($id);
        $languages = Language::all();

        foreach ($languages as $language) {

            if ($word->Translations->where('language_id', $language->id)->count() == 0) {
                $translation = new Translation();
                $translation->language_id = $language->id;
                $translation->language = $language;
                $translation->value = "";
                $word->Translations->add($translation);
            }

        }

        return view('words.edit')->with("word", $word);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, WordsBL $wordsBL)
    {
        return $wordsBL->delete($id)->toJson();
    }
}
