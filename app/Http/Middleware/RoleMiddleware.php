<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $roles =explode('|', $roles);


        if(Auth::check() && Auth::User()->HasRole($roles)) {

            return $next($request);
        }
        else {
            return redirect()->route('login');
        }
    }
}
