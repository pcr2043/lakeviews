<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('contacts', 'HomeController@contacts')->name('contacts');
Route::get('admin', 'HomeController@admin')->name('admin')->middleware('role:admin');

//Auth
Route::get('login', 'AuthController@login')->name('login');
Route::post('login', 'AuthController@authenticate')->name('login');
Route::get('logout', 'AuthController@logout')->name('logout');


//PagesController
Route::get('api/users/all', 'UsersController@all');
Route::resource('api/users', 'UsersController');

//PagesController
Route::get('api/pages/all', 'PagesController@all');
Route::resource('api/pages', 'PagesController');

//CustomersController
Route::get('dashboard', 'CustomersController@index');
Route::get('relocation', 'CustomersController@relocation');
Route::get('contracts', 'CustomersController@contracts');
Route::get('api/customer/contracts', 'CustomersController@customerContracts');
Route::post('api/customer/upload', 'CustomersController@customerUpload');
Route::get('api/customer/uploads', 'CustomersController@customerUploads');
Route::get('documents', 'CustomersController@documents');

//ContractsController
Route::resource('api/contracts/all', 'ContractsController@all');
Route::resource('api/contracts', 'ContractsController');

//LanguagesController
Route::resource('api/languages/{id}/change', 'LanguagesController@change');
Route::resource('api/languages/all', 'LanguagesController@all');
Route::resource('api/languages', 'LanguagesController');

//Insurance Types Controller
Route::resource('api/insurance-types/all', 'InsuranceTypesController@all');
Route::resource('api/insurance-types', 'InsuranceTypesController');

//Insurances Controller
Route::resource('api/insurances/all', 'InsurancesController@all');
Route::resource('api/insurances', 'InsurancesController');

//WordsController
Route::resource('api/words/all', 'WordsController@all');
Route::resource('api/words', 'WordsController');

//FilesController
Route::get('api/files/relocation/{id}', 'FilesController@relocation');
Route::get('api/files/contract/{id}', 'FilesController@contract');
Route::get('api/files/download/{id}', 'FilesController@download');
Route::delete('api/files/delete/{id}', 'FilesController@delete');

//MessagesController
Route::get('/api/messages/contact/send', 'MessagesController@sendContact');
Route::get('/api/messages/relocation', 'MessagesController@sendRelocation');
Route::get('/api/messages/contracts', 'MessagesController@sendContracts');

//SPEFICIATION FOR EDITOR
$middleware = array_merge(\Config::get('lfm.middlewares'), ['\Unisharp\Laravelfilemanager\middleware\MultiUser']);
$prefix = \Config::get('lfm.prefix', 'laravel-filemanager');
$as = 'unisharp.lfm.';
$namespace = '\Unisharp\Laravelfilemanager\controllers';

// make sure authenticated
Route::group(compact('middleware', 'prefix', 'as', 'namespace'), function () {

    // Show LFM
    Route::get('/', [
        'uses' => 'LfmController@show',
        'as' => 'show'
    ]);

    // upload
    Route::any('/upload', [
        'uses' => 'UploadController@upload',
        'as' => 'upload'
    ]);

    // list images & files
    Route::get('/jsonitems', [
        'uses' => 'ItemsController@getItems',
        'as' => 'getItems'
    ]);

    // folders
    Route::get('/newfolder', [
        'uses' => 'FolderController@getAddfolder',
        'as' => 'getAddfolder'
    ]);
    Route::get('/deletefolder', [
        'uses' => 'FolderController@getDeletefolder',
        'as' => 'getDeletefolder'
    ]);
    Route::get('/folders', [
        'uses' => 'FolderController@getFolders',
        'as' => 'getFolders'
    ]);

    // crop
    Route::get('/crop', [
        'uses' => 'CropController@getCrop',
        'as' => 'getCrop'
    ]);
    Route::get('/cropimage', [
        'uses' => 'CropController@getCropimage',
        'as' => 'getCropimage'
    ]);

    // rename
    Route::get('/rename', [
        'uses' => 'RenameController@getRename',
        'as' => 'getRename'
    ]);

    // scale/resize
    Route::get('/resize', [
        'uses' => 'ResizeController@getResize',
        'as' => 'getResize'
    ]);
    Route::get('/doresize', [
        'uses' => 'ResizeController@performResize',
        'as' => 'performResize'
    ]);

    // download
    Route::get('/download', [
        'uses' => 'DownloadController@getDownload',
        'as' => 'getDownload'
    ]);

    // delete
    Route::get('/delete', [
        'uses' => 'DeleteController@getDelete',
        'as' => 'getDelete'
    ]);

    Route::get('/demo', function () {
        return view('laravel-filemanager::demo');
    });
});



Route::get('{alias}', 'HomeController@alias')->name('alias');