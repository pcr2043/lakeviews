@extends('admin.master')

@section('content')

    <view-edit-users :user="{{ $user->ToJson() }}" inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="user.id > 0">EDIT USER</span>
                    <span v-if="user.id == -1">NEW USER</span>
                    <strong>@{{ user.first_name }}&nbsp;@{{ user.last_name }}</strong></legend>

                <div class="form-group col-md-3 h-90" v-bind:class="fields.first_name.class">
                    <label for="">FirstName</label>
                    <input type="text" class="form-control" v-model="user.first_name" placeholder="ex:. Antoine">
                    <span class="help-block" v-show="!fields.first_name.state" v-text="fields.first_name.msg"></span>
                </div>

                <div class="form-group col-md-3 h-90" v-bind:class="fields.last_name.class">
                    <label for="">LastName</label>
                    <input type="text" class="form-control" v-model="user.last_name" placeholder="ex:. Muller">
                    <span class="help-block" v-show="!fields.last_name.state" v-text="fields.last_name.msg"></span>
                </div>

                <div class="form-group col-md-3 h-90">
                    <label for="">Password</label>
                    <input type="password" class="form-control" v-model="user.password"
                           placeholder="ex:. User Password">
                </div>


                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">PERMITIONS</label>
                    <div>
                        <input v-on:click="user.type=2" type="checkbox" class="checkbox-primary"
                               :checked="user.type === 2">
                        <label for="checkbox1">CUSTOMER&nbsp;&nbsp;&nbsp;&nbsp;
                        </label>
                        <input class="checkbox-info" :checked="user.type === 1" type="checkbox"
                               v-on:click="user.type= 1">
                        <label for="checkbox1">ADMIN

                        </label>
                    </div>
                </div>

                <div class="form-group col-md-4 h-90" v-bind:class="fields.email.class">
                    <label for="">Email</label>
                    <input type="email" class="form-control" v-model="user.email"
                           placeholder="ex:. antoine.muller@lakeviews.ch">
                    <span class="help-block" v-show="!fields.email.state" v-text="fields.email.msg"></span>
                </div>

                <div class="form-group col-md-2 h-90">
                    <label for="">Date of Birth</label>
                    <input type="date" class="form-control" v-model="user.date_birth"
                           placeholder="ex:. enter date of Birth">
                </div>
                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">GENDER</label>
                    <div>
                        <input v-on:click="user.gender = 1" type="checkbox" class="checkbox-primary"
                               :checked="user.gender === 1">
                        <label for="checkbox1">MALE&nbsp;&nbsp;&nbsp;&nbsp;
                        </label>
                        <input class="checkbox-info" :checked="user.gender === 0" type="checkbox"
                               v-on:click="user.gender = 0">
                        <label for="checkbox1">FEMALE

                        </label>
                    </div>
                </div>


                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">ACTIVE</label>
                    <div>
                        <input v-on:click="user.active = 1" type="checkbox" class="checkbox-primary"
                               :checked="user.active === 1">
                        <label for="checkbox1">YES&nbsp;&nbsp;&nbsp;&nbsp;
                        </label>
                        <input class="checkbox-danger" :checked="user.active === 0" type="checkbox"
                               v-on:click="user.active = 0">
                        <label for="checkbox1">NO

                        </label>
                    </div>
                </div>
                <div class="form-group col-md-4 h-90">
                    <label for="">Country</label>
                    <select id="country" name="country" v-model="user.country" class="form-control">
                        <option value="AF">Afghanistan</option>
                        <option value="AX">Åland Islands</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AQ">Antarctica</option>
                        <option value="AG">Antigua and Barbuda</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia, Plurinational State of</option>
                        <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                        <option value="BA">Bosnia and Herzegovina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Côte d'Ivoire</option>
                        <option value="HR">Croatia</option>
                        <option value="CU">Cuba</option>
                        <option value="CW">Curaçao</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GG">Guernsey</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard Island and McDonald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran, Islamic Republic of</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IM">Isle of Man</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JE">Jersey</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="KE">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macao</option>
                        <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="ME">Montenegro</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PS">Palestinian Territory, Occupied</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Réunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="BL">Saint Barthélemy</option>
                        <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="LC">Saint Lucia</option>
                        <option value="MF">Saint Martin (French part)</option>
                        <option value="PM">Saint Pierre and Miquelon</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="RS">Serbia</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SX">Sint Maarten (Dutch part)</option>
                        <option value="SK">Slovakia</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="SS">South Sudan</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard and Jan Mayen</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH" selected>Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan, Province of China</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TL">Timor-Leste</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela, Bolivarian Republic of</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands, British</option>
                        <option value="VI">Virgin Islands, U.S.</option>
                        <option value="WF">Wallis and Futuna</option>
                        <option value="EH">Western Sahara</option>
                        <option value="YE">Yemen</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
                    </select>
                </div>


                <div class="form-group col-md-2 h-90">
                    <label for="">Best Time to call</label>
                    <input type="text" class="form-control" v-model="user.best_time_to_call"
                           placeholder="ex:. 17:00">

                </div>


                <div class="form-group col-md-3 h-90">
                    <label for="">Phone</label>
                    <input type="text" class="form-control" v-model="user.phone" placeholder="ex:. 079 123 123 12">
                </div>


                <div class="form-group col-md-3 h-90">
                    <label for="">Mobile</label>
                    <input type="text" class="form-control" v-model="user.mobile" placeholder="ex:. 079 123 123 12">
                </div>

                <div class="form-group col-md-7 h-90">
                    <label for="">Address</label>
                    <input type="text" class="form-control" v-model="user.address"
                           placeholder="ex:. Rue de La Praille">
                </div>
                <div class="form-group col-md-3 h-90">
                    <label for="">City</label>
                    <input type="text" class="form-control" v-model="user.city" placeholder="ex:. Genéve">
                </div>
                <div class="form-group col-md-2 h-90">
                    <label for="">NPA</label>
                    <input type="text" class="form-control" v-model="user.npa" placeholder="ex:. 1200">

                </div>

                <div class="form-group col-md-12 h-90">
                    <label for="">NOTES</label>
                    <textarea rows="3" class="form-control wp100" v-model="user.notes" placeholder="ex:. Notes">
                        </textarea>

                </div>

                <div class="form-group col-md-12 h-90 mg-top-50">
                    <label for="" class="w100p">REALOCATION</label>
                    <div>
                        <input v-on:click="user.relocation = 1" type="checkbox" class="checkbox-primary"
                               :checked="user.relocation === 1">
                        <label for="checkbox1">YES&nbsp;&nbsp;&nbsp;&nbsp;
                        </label>
                        <input class="checkbox-danger" :checked="user.relocation === 0" type="checkbox"
                               v-on:click="user.relocation = 0">
                        <label for="checkbox1">NO

                        </label>
                    </div>
                </div>
                <div class="col-xs-8" v-bind:class="(user.relocation === 1) ? 'visible' : 'hidden'">
                    <div class="form-group col-md-6 h-90 no-padding">
                        <label for="">COMPANY</label>
                        <input type="text" class="form-control" v-model="user.relocation_company"
                               placeholder="ex:. 1200">

                    </div>
                    <div class="form-group col-md-6 h-90 no-padding">
                        <label for="">EMAIL</label>
                        <input type="email" class="form-control" v-model="user.relocation_email"
                               placeholder="ex:. 1200">

                    </div>

                    <div class="form-group col-md-12 h-90 no-padding">
                        <label for="">Address</label>
                        <input type="text" class="form-control" v-model="user.relocation_address"
                               placeholder="ex:. Geneve, nº7 - 1200 Carouge">

                    </div>

                    <div class="form-group col-md-6 h-90 no-padding">
                        <label for="">Latitude</label>
                        <input type="text" class="form-control" v-model="user.relocation_latitude"
                               placeholder="ex:.-6.3572375290155">

                    </div>

                    <div class="form-group col-md-6 h-90 no-padding">
                        <label for="">Longitude</label>
                        <input type="text" class="form-control" v-model="user.relocation_longitude"
                               placeholder="ex:. -6.3572375290155">

                    </div>

                    <div class="form-group col-md-12 no-padding">
                        <label class="wp100" for="">COMMENTS</label>
                        <ckeditor id="relocation_text" name="relocation_text"
                                  :field.sync="user.relocation_text"
                                  toolbar="default"
                                  heigth="300"
                        ></ckeditor>
                    </div>
                </div>
                <div class="col-xs-4" v-bind:class="(user.relocation === 1) ? 'visible' : 'hidden'">
                    <label for="">Uploads</label>
                    <input type="file" name="uploads" id="uploads" multiple="multiple">
                    <div class="list-group mg-top-20">
                        <div v-for="file in files" class="list-group-item list-group-item-download wp100">
                            <i v-on:click="removeStorage(file)" class="fa fa-trash remove" aria-hidden="true"></i>
                            @{{ file.name }}
                            <a v-bind:href="'/api/files/download/' + file.id"><i class="fa fa-cloud-download download"
                                                                                 aria-hidden="true"></i></a>
                        </div>
                        <div v-for="file in uploads" class="list-group-item list-group-item-download wp100">
                            <i v-on:click="remove(file)" class="fa fa-trash remove" aria-hidden="true"></i>
                            @{{ file.name }}
                            <a><i v-on:click="download(file)" class="fa fa-cloud-download download"
                                  aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>


                <div class="form-group col-xs-12 tar">
                    <a href="/api/users" class="btn btn-secondary">CLOSE</a>
                    <button type="submit" class="btn btn-primary">
                        <i v-bind:class="(loading) ? 'visible' : 'hidden'" id="btn_loader"
                           class="fa fa-gear faa-spin animated hidden"></i>
                        SAVE
                    </button>
                </div>
            </form>
        </div>
    </view-edit-users>
@endsection