@extends('admin.master')

@section('content')

    <view-users inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <div class="list-group list-table">
                {{-- HEADER TITLES --}}
                <div class="list-group-item header">
                    <div class="col-md-1">#</div>
                    <div class="col-md-4">Name</div>
                    <div class="col-md-3">Email</div>
                    <div class="col-md-2">Type User</div>
                    <div class="col-md-2 tar">
                        <a href="/api/users/-1/edit" type="button" class="btn btn-sm btn-secondary"
                        ><i class="fa fa-plus" aria-hidden="true"></i> NEW</a>

                    </div>
                </div>

                <div class="list-group-item filters">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" v-model="params.filters.name" v-on:keyup="rebind()"
                               placeholder="type a name" ></input>
                    </div>
                    <div class="col-md-3"><input type="text" class="form-control" v-model="params.filters.email"
                                                 placeholder="type a alias"
                                                 v-on:keyup="rebind()"/>
                    </div>

                    <div class="col-md-2">

                        <select class="form-control" v-model="params.filters.type"  v-on:change="rebind()">
                            <option value="">All</option>
                            <option value="2">Customers</option>
                            <option value="1">Administrators</option>
                        </select>

                    </div>
                    <div class="col-md-2"></div>

                </div>

                {{-- DATA --}}

                <div v-for="item in data" class="list-group-item item">
                    <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.id }}</div>
                    <div class="col-md-4 dinamic"><span class="m-title">Name: </span>@{{ item.name }}</div>
                    <div class="col-md-3 dinamic"><span class="m-title">Email: </span>@{{ item.email }}</div>
                    <div class="col-md-2 dinamic"><span class="m-title">Type: </span>
                        <span v-if="item.type == 1" class="label label-info">ADMIN</span>
                        <span v-if="item.type == 2" class="label label-primary">CUSTOMER</span>
                    </div>
                    <div class="col-md-2 tar item-controls">
                        <a v-bind:href="'/api/users/' + item.id + '/edit'" type="button" class="btn btn-sm btn-primary">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a v-on:click="removeConfirm(item)" type="button" class="btn btn-sm btn-danger">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                {{--FOOTER--}}
                <div class="list-group-item list-group-item-primary footer">
                    <div class="col-xs-12 col-md-4">

                        <div class="btn-group">
                            <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''"
                               v-on:click="
                        changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
                        </div>


                    </div>
                    <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
                        <div v-if="page.pages.length > 1" class="btn-group">
                            <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages"
                               v-on:click="
                        changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </view-users>
@endsection