@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2>{{ $page->name }}</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / {!!  $page->getPath() !!}
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <div class="container">
            @if(!empty($page->name))
                {!!$page->content !!}

            @else
               <h3>PAGE NOT FOUND</h3>
            @endif
    </div>
    <div class="space70"></div>

@endsection