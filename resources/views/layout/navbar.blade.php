<nav class="navbar navbar-inverse navbar-static-top yamm">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="images/logo-light.png" alt=""></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                @if(empty($page))
                    {{ $page = null }}
                @endif

                @foreach($utils->Pages() as  $spage)
                    {{ $spage->tree($spage, $page, 0) }}
                @endforeach
                <li><a href="/contacts">CONTACTS</a></li>
            </ul>
        </div>
    </div>
</nav>