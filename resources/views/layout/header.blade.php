<div class="top-bar blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
                <span><i class="fa fa-phone"></i>  +41 22 772 2211</span>
                <span><i class="fa fa-envelope"></i> INFO@LAKEVIEWS.CH</span>

            </div>
            <div class="col-sm-2 hidden-xs languages">
                @include('layout.languages')
            </div>
            <div class="col-sm-6 text-right user-menu">
                <span><a href="/"><i class="fa fa-lg fa-home"></i></a></span>
                @if(Auth::check())
                    @if(Auth::User()->isAdmin())
                        <span><a href="http://{{ Request::server('HTTP_HOST') }}/admin" target="_blank"><i
                                        class="fa fa-tachometer" aria-hidden="true"></i></a></span>
                    @endif
                    <span><a href="/dashboard"><i
                                    class="fa fa-archive" aria-hidden="true"></i></a></span>
                    <span><strong>{{ $utils->translate('welcome') }}</strong>
                            ,&nbsp;&nbsp;{{ Auth::User()->fullname() }} &nbsp; <a style="display: inline-block" href="/logout">Logout</a></span>
                @else
                    <span class="login"><a href="/login"><i class="fa fa-power-off" aria-hidden="true"></i>Login</a></span>
                @endif
            </div>
        </div>
    </div>
</div>

