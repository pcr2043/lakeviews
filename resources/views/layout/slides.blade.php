<div class="flexslider basicslider">
    <ul class="slides">
        <li>
            <article>
                <p>{{ $utils->translate('welcome') }}</p>
                <h3 class="heading">LAKEVIEWS</h3>
                <p>{{ $utils->translate('quote') }}</p>

            </article>
        </li>

    </ul>
</div>