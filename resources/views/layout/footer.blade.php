<footer class="footer" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 margin-btm-30">
                <h3>SITEMAP</h3>
                <ul class="list-unstyled f-list page-list">
                    @foreach($utils->Pages() as  $spage)
                        <li><a href="/{{ $spage->alias }}">{{ $spage->name }}</a></li>
                    @endforeach
                    <li><a href="/contacts">CONTACTS</a></li>
                </ul>
            </div>
            <div class="col-md-offset-6 col-md-3 margin-btm-30 ">
                <h3>Contact us</h3>
                <ul class="list-inline footer-social">
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                </ul>
                <p class="phone"><i class="fa fa-envelope"></i>info@lakeviews.ch</p>
                <p class="phone"><i class="fa fa-phone"></i> +41 22 772 2211</p>
                <p> RUE BLAVIGNAC, 10</br>
                    CH - 1227 Carouge</br>
                    Geneve</p>
                <p>{{  $utils->translate('open-hours') }}</p>

            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <span>&copy; Copyright 2017. All Right Reserved. LAKEVIEWS</span>
        </div>
    </div>
</footer>