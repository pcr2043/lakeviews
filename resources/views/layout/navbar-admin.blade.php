<div class="navbar navbar-default navbar-admin">
    <div class="container-fluid">
        <div class="navbar-header">

            <div class="logo2">
                <a href="../" class="navbar-brand"></a>
            </div>

            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav nav-icons">
                @if(strpos(Route::getCurrentRoute()->getPath(),'pages'))
                    <li class="active">
                @else
                    <li>
                        @endif
                        <a href="/api/pages">
                            <i class="fa fa-file" aria-hidden="true"></i>
                            <div class="title">PAGES </div>

                        </a>
                    </li>
                    @if(strpos(Route::getCurrentRoute()->getPath(),'users'))
                        <li class="active">
                    @else
                        <li>
                            @endif
                        <a href="/api/users">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <div class="title">USERS</div>

                        </a>
                    </li>
                        @if(strpos(Route::getCurrentRoute()->getPath(),'contracts'))
                            <li class="active">
                        @else
                            <li>
                                @endif
                        <a href="/api/contracts">
                            <i class="fa fa-file-text" aria-hidden="true"></i>
                            <div class="title">CONTRACTS</div>

                        </a>
                    </li>
                            @if(strpos(Route::getCurrentRoute()->getPath(),'insurance-types'))
                                <li class="active">
                            @else
                                <li>
                                    @endif
                        <a href="/api/insurance-types">
                            <i class="fa fa-archive" aria-hidden="true"></i>
                            <div class="title">INSURANCES TYPES</div>
                        </a>
                    </li>
                                @if(strpos(Route::getCurrentRoute()->getPath(),'insurances'))
                                    <li class="active">
                                @else
                                    <li>
                                        @endif
                        <a href="/api/insurances">
                            <i class="fa fa-umbrella" aria-hidden="true"></i>
                            <div class="title">INSURANCES</div>
                        </a>
                    </li>
                                    @if(strpos(Route::getCurrentRoute()->getPath(),'languages'))
                                        <li class="active">
                                    @else
                                        <li>
                                            @endif
                        <a href="/api/languages">
                            <i class="fa fa-language" aria-hidden="true"></i>
                            <div class="title">LANGUAGES</div>
                        </a>
                    </li>
                                        @if(strpos(Route::getCurrentRoute()->getPath(),'words'))
                                            <li class="active">
                                        @else
                                            <li>
                                                @endif
                        <a href="/api/words">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <div class="title">WORDS</div>
                        </a>
                    </li>
            </ul>
            <ul class="nav navbar-nav navbar-right user-control">
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            <span class="user-welcome"><strong>{{ $utils->translate('welcome') }}</strong>,&nbsp;&nbsp;{{ Auth::User()->fullname() }}</span>
                            <img class="user-logo" src="../../../img/icons/no_avatar.png" alt="user-ico"></img>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/">Home</a></li>
                            @if(Auth::User()->hasRole(['admin']))
                                <li class="divider"></li>
                                <li><a href="/admin">Admin</a></li>
                            @endif
                            <li class="divider"></li>
                            <li class="logout"><a href="/logout"><i class="fa fa-power-off" aria-hidden="true"></i>
                                    Logout</a></li>
                        </ul>
                    </li>
                @else
                    <li class="user-auth-control">
                        <a href="/login"><i class="fa fa-lock" aria-hidden="true"></i>LOGIN
                            <div class="line"></div>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>