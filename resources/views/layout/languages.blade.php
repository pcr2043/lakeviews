@if(count($utils->languages()) > 1)
    @foreach($utils->languages() as $language)
        @if($language->id == $utils->defaultLanguage()->id)
            <span class="active" ><a href="api/languages/{{ $language->id }}/change">{{ $language->code }}</a></span>
        @else
            <span><a  href="api/languages/{{ $language->id }}/change">{{ $language->code }}</a></span>
        @endif
    @endforeach
@endif