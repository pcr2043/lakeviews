@extends('admin.master')

@section('content')

    <view-edit-pages :page="{{ $page->ToJson() }}" inline-template>
        <div class="col-xs-12">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="page.id > 0">EDIT PAGE</span>
                    <span v-if="page.id == -1">NEW PAGE</span>
                    <strong>@{{ page.name }}</strong></legend>

                <div class="form-group col-md-6 h-90" v-bind:class="fields.name.class">
                    <label for="">Name</label>
                    <input type="text" class="form-control" v-model="page.name" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
                </div>
                <div class="form-group col-md-3 h-90">
                    <label class="wp100" for="">Language</label>
                    <select class="form-control" v-model="page.language_id">
                        @foreach($languages as $language)
                            <option value="{{ $language->id }}">{{ $language->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">Active</label>
                    <div>
                        <input v-on:click="page.active = 1" type="checkbox"
                               :checked="page.active === 1">
                        <label for="checkbox1">YES&nbsp;&nbsp;&nbsp;&nbsp;

                        </label>

                        <input class="checkbox-danger" :checked="page.active === 0" type="checkbox" v-on:click="page.active = 0">
                        <label for="checkbox1">NO

                        </label>
                    </div>
                </div>

                <div class="form-group col-md-4 h-90" v-bind:class="fields.alias.class">
                    <label for="">Alias</label>
                    <input type="text" class="form-control" v-model="page.alias" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.alias.state" v-text="fields.alias.msg"></span>
                </div>
                <div class="form-group col-md-2 h-90">
                    <label for="">Order</label>
                    <input type="number" class="form-control" v-model="page.order" placeholder="ex:. home">

                </div>

                <div class="form-group col-md-3 h-90">
                    <label class="wp100" for="">Parent</label>
                    <select class="form-control" v-model="page.parent">
                        <option value="">NONE</option>
                        @foreach($pages as $page)
                            <option value="{{ $page->id }}">{{ $page->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">Menu</label>
                    <div>
                        <input v-on:click="page.menu = 1" type="checkbox"
                               :checked="page.menu === 1">
                        <label for="checkbox1">YES&nbsp;&nbsp;&nbsp;&nbsp;
                        </label>
                        <input class="checkbox-danger" :checked="page.menu === 0" type="checkbox" v-on:click="page.menu = 0">
                        <label for="checkbox1">NO

                        </label>
                    </div>
                </div>


                <div class="form-group col-md-12">
                    <label class="wp100" for="">Content</label>
                    <ckeditor id="page" name="page"
                              :field.sync="page.content"
                              toolbar="default"
                              heigth="300"
                    ></ckeditor>
                </div>
                <div class="form-group col-xs-12 tar">
                    <a  href="/api/pages" class="btn btn-secondary">CLOSE</a>
                    <button type="submit" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
    </view-edit-pages>
@endsection