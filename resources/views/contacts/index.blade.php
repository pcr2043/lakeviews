@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2>CONTACTS</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / <a href="/contacts">CONTACTS</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <div class="container">
        <div class="row">

            <div id="msg_result" class="alert alert-success message hidden">
                <strong>{{ $utils->translate('message-sent') }}!</strong>
            </div>
            <div id="validate_form" class="alert alert-danger message hidden">
                <strong>{{ $utils->translate('verify-form') }}!</strong>
            </div>

            <div class="col-md-7">
                <form method="POST" id="contact_form" role="form">
                    <div class="form-group">
                        <label for="">{{ $utils->translate('name') }}</label>

                        <input type="text" class="form-control" name="name" id="name"
                               placeholder="{{ $utils->translate('enter-your-name') }}">
                        <span style="top:27px" class="input-icon"><i class=" fa fa-user"></i></span>

                    </div>
                    <div class="form-group">
                        <label for="">{{ $utils->translate('email') }}</label>

                        <input type="text" class="form-control"  name="email" id="email"
                               placeholder="{{ $utils->translate('enter-your-email') }}">

                        <span style="top:27px" class="input-icon"><i class=" fa fa-envelope"></i></span>
                    </div>
                    <div class="form-group">
                        <label for="">{{ $utils->translate('mobile') }}</label>

                        <input type="text" class="form-control" name="mobile" id="mobile"

                               placeholder="{{ $utils->translate('enter-your-mobile') }}">

                        <span style="top:27px" class="input-icon"><i class=" fa fa-phone"></i></span>
                    </div>
                    <div class="form-group">
                        <label for="">{{ $utils->translate('best-time-to-call') }}</label>

                        <input type="text" class="form-control" name="time_call" id="time_call" mobile="time_call"

                               placeholder="{{ $utils->translate('enter-best-time-to-call') }}">
                        <span style="top:27px" class="input-icon"><i class=" fa fa fa-clock-o"></i></span>
                    </div>
                    <div class="form-group">
                        <label for="">{{ $utils->translate('message') }}</label>

                        <textarea rows="4" class="form-control" name="message" id="message"

                                  placeholder="{{ $utils->translate('enter-your-message') }}"></textarea>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group form-footer tar">
                        <button type="submit" class="btn btn-lg btn-block btn-skin">
                            <i id="btn_loader" class="fa fa-gear faa-spin animated hidden"></i>
                            {{ $utils->translate('send') }}
                        </button>
                    </div>
                </form>

            </div>
            <div class="col-md-5">
                <h3><strong>LAKEVIEWS GWM SA</strong></h3>
                RUE BLAVIGNAC, 10</br>
                CH - 1227 Carouge</br>
                Tel: +41 22 772 2211</br>
                <div id="map" style="    width: 100%;
    height: 370px;"></div>
                <script src=" https://maps.googleapis.com/maps/api/js?callback=initMap"
                        async defer></script>
                <script type="text/javascript">
                    function initMap() {

                        var mapDiv = document.getElementById('map');
                        var map = new google.maps.Map(mapDiv, {
                            center: {lat: 46.182018, lng: 6.133553},
                            zoom: 15
                        });
                    }
                </script>
            </div>

        </div>
    </div>
    <div class="space70"></div>
    <script>

        $(document).ready(function () {

            $("#contact_form").submit(function (e) {

                var message = new Object();
                message.name = $("#name").val();
                message.email = $("#email").val();
                message.mobile = $("#mobile").val()
                message.availability = $("#time_call").val();
                message.message = $("#message").val();

                if (message.name.length > 0 && message.availability.length && message.email.length > 0 && message.message.length > 0) {

                    $("#btn_loader").removeClass("hidden")
                    $.get("/api/messages/contact/send", message).done(function () {

                        $("#btn_loader").addClass("hidden")
                        $("#msg_result").removeClass("hidden");

                        setTimeout(function () {
                            $("#msg_result").addClass("hidden");
                        }, 5000);
                    })

                    e.preventDefault();
                }
                else {
                    $("#validate_form").removeClass("hidden")
                    setTimeout(function () {
                        $("#validate_form").addClass("hidden");
                    }, 3000);
                }
                return false;
            })

        });
    </script>
@endsection
