<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAKEVIEWS</title>

    <link rel="icon" type="image/vnd.microsoft.icon"
          href="http://www.lakeviews.ch/img/favicon.ico" />

    <link href="theme/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/css/animate.css" rel="stylesheet">
    <link href="theme/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="theme/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="theme/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="theme/owl-carousel/owl.transitions.css" rel="stylesheet">
    <link href="theme/flexslider/flexslider.css" rel="stylesheet">
    <!--Revolution slider css-->
    <link href="theme/rs-plugin/css/settings.css" rel="stylesheet" type="text/css" media="screen">
    <link href="theme/css/style.css" rel="stylesheet">


    <link href="css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<script>
    window.Laravel =
            <?php echo json_encode([
                    'csrfToken' => csrf_token(),
            ]); ?>

                    z = null;
</script>

<body>
<div id="app" v-bind:class="(loaded) ? 'app-visible' : 'app-hidden'" class="app-hidden">
@include('layout.header')
    <!-- Static navbar -->
@include('layout.navbar')
<!--/nav end -->
    <!--rev slider start-->

    <div class="fullwidthbanner">
        <div class="tp-banner">
            <ul>
                <!-- SLIDE -->
                <li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="{{ $utils->translate('insurances')  }}">
                    <!-- MAIN IMAGE -->
                    <img src="images/b1.png" alt="darkblurbg" data-bgfit="cover" data-bgposition="left top"
                         data-bgrepeat="no-repeat">
                    <div class="caption slider-title dark sft"
                         data-x="50"
                         data-y="160"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="easeOutExpo">
                        {!!  $utils->translate('insurances.title') !!}
                    </div>


                    <div class="caption slider-caption dark sfl"
                         data-x="50"
                         data-y="310"
                         data-speed="1000"
                         data-start="1800"
                         data-easing="easeOutExpo">
                        {!!   $utils->translate('insurances.quote') !!}
                    </div>
                    <div class="caption sfb rev-buttons tp-resizeme"
                         data-x="50"
                         data-y="380"
                         data-speed="500"
                         data-start="1800"
                         data-easing="Sine.easeOut">
                        <a href="/insurances" class="btn btn-skin btn-lg">{{ $utils->translate('insurances.button') }} <i class="fa fa fa-umbrella"></i></a>
                    </div>


                </li>
                <!-- SLIDE -->
                <li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="{{ $utils->translate('heritage')  }}">
                    <!-- MAIN IMAGE -->
                    <img src="images/b2.png" alt="darkblurbg" data-bgfit="cover" data-bgposition="left top"
                         data-bgrepeat="no-repeat">


                    <div class="caption slider-title light sft"
                         data-x="50"
                         data-y="160"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="easeOutExpo">
                        {!!  $utils->translate('heritage.title') !!}

                    </div>


                    <div class="caption slider-caption light sfl"
                         data-x="50"
                         data-y="310"
                         data-speed="1000"
                         data-start="1800"
                         data-easing="easeOutExpo">
                        {!!   $utils->translate('heritage.quote') !!}
                    </div>
                    <div class="caption sfb rev-buttons tp-resizeme"
                         data-x="50"
                         data-y="380"
                         data-speed="500"
                         data-start="1800"
                         data-easing="Sine.easeOut">
                        <a href="/heritage" class="btn btn-skin btn-lg">{{ $utils->translate('heritage.button') }} <i class="fa fa-university"></i></a>
                    </div>
                </li>
                <!-- SLIDE -->
                <li data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="{{ $utils->translate('other-services')  }}">
                    <!-- MAIN IMAGE -->
                    <img src="images/b3.png" alt="darkblurbg" data-bgfit="cover" data-bgposition="left top"
                         data-bgrepeat="no-repeat">


                    <div class="caption slider-title dark sft"
                         data-x="50"
                         data-y="200"
                         data-speed="1000"
                         data-start="1000"
                         data-easing="easeOutExpo">
                        {!!  $utils->translate('services.title') !!}

                    </div>


                    <div class="caption slider-caption dark sfl"
                         data-x="50"
                         data-y="290"
                         data-speed="1000"
                         data-start="1800"
                         data-easing="easeOutExpo">
                        {!!  $utils->translate('services.quote') !!}
                    </div>
                    <div class="caption sfb rev-buttons tp-resizeme"
                         data-x="50"
                         data-y="360"
                         data-speed="500"
                         data-start="1800"
                         data-easing="Sine.easeOut">
                        <a href="/other-services" class="btn btn-skin btn-lg">{{ $utils->translate('services.button') }} <i class="fa fa-cogs"></i></a>
                    </div>

                </li>
            </ul>
        </div>
    </div><!--full width banner-->
    <!--revolution end-->
    <div class="dinamic-seperator-50"></div>
    <!--end call to action-->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 margin-btm-30">
                <div class="green-boxe" v-on:click="getUrl('lakeviews')">
                    <i class="fa fa-home"></i>
                    <h3>LAKEVIEWS</h3>
                    <p>
                        {{ $utils->translate('lakeviews.details') }}
                    </p>
                    <div class="details">{{ $utils->translate('more-details') }}<i class="fa fa-caret-right"></i>
                    </div>
                </div><!--Blue Boxes-->
            </div><!--Col-->
            <div class="col-md-3 col-sm-6 margin-btm-30">
                <div class="green-boxe" v-on:click="getUrl('insurances')">
                    <i class="fa fa-umbrella"></i>
                    <h3>{{ $utils->translate('insurances') }}</h3>
                    <p>
                        {{ $utils->translate('insurances.details') }}
                    </p>
                    <div class="details">{{ $utils->translate('more-details') }}<i class="fa fa-caret-right"></i>
                    </div>
                </div><!--Blue Boxes-->
            </div><!--Col-->
            <div class="col-md-3 col-sm-6 margin-btm-30">
                <div class="green-boxe" v-on:click="getUrl('heritage')">
                    <i class="fa fa-university" aria-hidden="true"></i>
                    <h3>{{ $utils->translate('heritage') }}</h3>
                    <p>
                        {{ $utils->translate('heritage.details') }}
                    </p>
                    <div class="details">{{ $utils->translate('more-details') }}<i class="fa fa-caret-right"></i>
                    </div>
                </div><!--Blue Boxes-->
            </div><!--Col-->
            <div class="col-md-3 col-sm-6 margin-btm-30">
                <div class="green-boxe" v-on:click="getUrl('other-services')">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <h3>{{ $utils->translate('other-services') }}</h3>
                    <p>
                        {{ $utils->translate('services.details') }}
                    </p>
                    <div class="details">{{ $utils->translate('more-details') }}<i class="fa fa-caret-right"></i>
                    </div>
                </div><!--Blue Boxes-->
            </div><!--Col-->
        </div><!--row-->
    </div><!--container-->
    <div class="space40"></div>


    <!--footer start-->
    @include("layout.footer")
</div>
<!-- JAVASCRIPTS -->


<script src="js/app.js"></script>
<script src="theme/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="theme/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script type="text/javascript" src="theme/js/revolution-custom.js"></script>

</body>
</html>