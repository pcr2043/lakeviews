@extends('admin.master')

@section('content')

    <view-insurance-types inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <div class="list-group list-table">
                {{-- HEADER TITLES --}}
                <div class="list-group-item header">
                    <div class="col-md-1">#</div>
                    <div class="col-md-4">Name</div>
                    <div class="col-md-1">Order</div>
                    <div class="col-md-4">Reference</div>
                    <div class="col-md-2 tar">
                        <a href="/api/insurance-types/-1/edit" type="button" class="btn btn-sm btn-secondary"
                        ><i class="fa fa-plus" aria-hidden="true"></i> NEW</a>
                    </div>
                </div>

                <div class="list-group-item filters">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" v-model="params.filters.name" v-on:keyup="rebind()"
                               placeholder="type a name" ></input>
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.order" v-on:keyup="rebind()"
                               placeholder="1" ></input>
                    </div>
                    <div class="col-md-4"><input type="text" class="form-control" v-model="params.filters.reference"
                                                 placeholder="type a reference"
                                                 v-on:keyup="rebind()"/>
                    </div>

                    <div class="col-md-2"></div>

                </div>

                {{-- DATA --}}

                <div v-for="item in data" class="list-group-item item">
                    <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.id }}</div>
                    <div class="col-md-4 dinamic"><span class="m-title">NAME: </span>@{{ item.name }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">ORDER: </span>@{{ item.order }}</div>
                    <div class="col-md-4 dinamic"><span class="m-title">REFERENCE: </span>@{{ item.reference }}</div>
                    <div class="col-md-2 tar item-controls">
                        <a v-bind:href="'/api/insurance-types/' + item.id + '/edit'" type="button" class="btn btn-sm btn-primary">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a v-on:click="removeConfirm(item)" type="button" class="btn btn-sm btn-danger">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                {{--FOOTER--}}
                <div class="list-group-item list-group-item-primary footer">
                    <div class="col-xs-12 col-md-4">

                        <div class="btn-group">
                            <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''"
                               v-on:click="
                        changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
                        </div>


                    </div>
                    <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
                        <div v-if="page.pages.length > 1" class="btn-group">
                            <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages"
                               v-on:click="
                        changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </view-insurance-types>
@endsection