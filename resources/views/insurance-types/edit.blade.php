@extends('admin.master')

@section('content')

    <view-edit-insurance-types :insurance_type="{{ $insurance_type->ToJson() }}" inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="insurance_type.id > 0">EDIT TYPE</span>
                    <span v-if="insurance_type.id == -1">NEW TYPE</span>
                    <strong>@{{ insurance_type.name }}</strong></legend>

                <div class="form-group col-md-6 h-90" v-bind:class="fields.name.class">
                    <label for="">Name</label>
                    <input type="text" class="form-control" v-model="insurance_type.name" placeholder="ex:. Health">
                    <span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
                </div>
                <div class="form-group col-md-4 h-90" v-bind:class="fields.reference.class">
                    <label for="">Reference</label>
                    <input type="text" class="form-control" v-model="insurance_type.reference" placeholder="ex:. health">
                    <span class="help-block" v-show="!fields.name.state" v-text="fields.reference.msg"></span>
                </div>
                <div class="form-group col-md-2 h-90">
                    <label for="">Order</label>
                    <input type="text" class="form-control" v-model="insurance_type.order" placeholder="ex:. 1">
                </div>
                <div class="form-group col-xs-12 tar">
                    <a  href="/api/insurance-types" class="btn btn-secondary">CLOSE</a>
                    <button type="submit" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
    </view-edit-insurance-types>
@endsection