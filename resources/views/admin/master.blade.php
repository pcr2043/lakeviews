<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAKEVIEWS - ADMINISTRATION</title>
    <link rel="stylesheet" href="../../../css/app.css">

</head>
<script>
    window.Laravel =
    <?php echo json_encode([
            'csrfToken' => csrf_token(),
    ]); ?>

    z=null;
</script>
<body>
<div id="app">

    @include('layout/navbar-admin')

    <div class="container app-admin-container">
        <div class="col-xs-12 col-sm-6 col-md-4 noty">
            <noty v-for="noty in notifications" :noty="noty"></noty>
        </div>
        <div class="view">
            @yield('content')
        </div>
    </div>
</div>
{{--Ckeditor--}}
<script src="../../../vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
{{--Color Picker--}}
<link rel="stylesheet" href="../../../plugins/color-picker/dist/css/bootstrap-colorpicker.min.css">
{{--Vue App--}}
<script type="text/javascript" src="../../../js/admin.js"></script>
</body>
</html>