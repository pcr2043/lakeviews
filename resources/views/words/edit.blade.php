@extends('admin.master')

@section('content')

    <view-edit-words :word="{{ $word->ToJson() }}" inline-template>
        <div class="col-xs-12">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="word.id > 0">EDIT WORD</span>
                    <span v-if="word.id == -1">NEW WORD</span>
                    <strong>@{{ word.reference }}</strong></legend>

                <div class="form-group col-md-6 h-90" v-bind:class="fields.reference.class">
                    <label for="">Reference</label>
                    <input type="text" class="form-control" v-model="word.reference" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.reference.state" v-text="fields.reference.msg"></span>
                </div>

                <div class="form-group col-md-6 h-90" v-bind:class="fields.value.class">
                    <label for="">Default Value</label>
                    <input type="text" class="form-control" v-model="word.value" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.value.state" v-text="fields.value.msg"></span>
                </div>
                <div class="form-group col-xs-12">
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">

                                <li v-for="translation in word.translations" v-bind:class="$index== 0 ? 'active' : ''">
                                    <a v-bind:href="'#tab-'+translation.language_id"
                                       data-toggle="tab">@{{ translation.language.name }}</a></li>

                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div v-for="translation in word.translations" v-bind:class="$index== 0 ? 'active' : ''"

                                     class="tab-pane" v-bind:id="'tab-'+translation.language_id">

                                    <input type="text" class="form-control" v-model="translation.value"
                                           v-bind:placeholder="'Type translation in ' +  translation.language.name">

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 tar">
                        <a href="/api/words" class="btn btn-secondary">CLOSE</a>
                        <button type="submit" class="btn btn-primary">SAVE</button>
                    </div>
            </form>
        </div>
    </view-edit-words>
@endsection