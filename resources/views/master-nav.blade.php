<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LAKEVIEWS</title>

    <link rel="icon" type="image/vnd.microsoft.icon"
          href="http://www.lakeviews.ch/img/favicon.ico" />

    <link href="theme/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/css/animate.css" rel="stylesheet">
    <link href="theme/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="theme/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="theme/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="theme/owl-carousel/owl.transitions.css" rel="stylesheet">
    <link href="theme/flexslider/flexslider.css" rel="stylesheet">
    <!--Revolution slider css-->
    <link href="theme/rs-plugin/css/settings.css" rel="stylesheet" type="text/css" media="screen">
    <link href="theme/css/style.css" rel="stylesheet">

    <script src="theme/js/jquery.min.js" type="text/javascript"></script>
    {{--Ckeditor--}}
    <script src="../../../vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <link href="css/layout.css" rel="stylesheet" type="text/css" media="all">
</head>
<script>
    window.Laravel =
            <?php echo json_encode([
                    'csrfToken' => csrf_token(),
            ]); ?>

                    z = null;
</script>

<body>
<div id="app" v-bind:class="(loaded) ? 'app-visible' : 'app-hidden'" class="app-hidden">
@include('layout.header')
<!-- Static navbar -->
@include('layout.navbar')
<!--/nav end -->

    <div class="dinamic-seperator-50"></div>
    <!--end call to action-->
@yield("content")


<!--footer start-->
    @include("layout.footer")
</div>
<!-- JAVASCRIPTS -->


<script src="js/app.js"></script>
<script src="theme/rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="theme/rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script type="text/javascript" src="theme/js/revolution-custom.js"></script>

</body>
</html>