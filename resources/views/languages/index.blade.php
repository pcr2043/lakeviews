@extends('admin.master')

@section('content')

    <view-languages inline-template>
        <div class="col-xs-12">
            <div class="list-group list-table">
                {{-- HEADER TITLES --}}
                <div class="list-group-item header">
                    <div class="col-md-1">#</div>
                    <div class="col-md-3">Name</div>
                    <div class="col-md-2">Code</div>
                    <div class="col-md-2 tac">Active</div>
                    <div class="col-md-2 tac">Default</div>
                    <div class="col-md-2 tar">
                        <a href="/api/languages/-1/edit" type="button" class="btn btn-sm btn-secondary"
                        ><i class="fa fa-plus" aria-hidden="true"></i> NEW</a>

                    </div>
                </div>

                <div class="list-group-item filters">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" v-model="params.filters.name" v-on:keyup="rebind()" placeholder="type a name"></input>
                    </div>
                    <div class="col-md-2"><input type="text" class="form-control" v-model="params.filters.code" placeholder="type a code"
                                                 v-on:keyup="rebind()"/>
                    </div>
                    <div class="col-md-2 tac">
                        <div class="btn-group" role="group">
                            <a href="#" v-on:click="changeActive('')"
                               v-bind:class="(params.filters.active === '') ? 'active' : ''" class="
                        btn btn-xs btn-default">ALL </a>
                            <a href="#" v-on:click="changeActive(1)"
                               v-bind:class="(params.filters.active === 1) ? 'active' : ''" class="
                        btn btn-xs btn-success btn-default">ON</a>
                            <a href="#" v-on:click="changeActive(0)"
                               v-bind:class="(params.filters.active === 0) ? 'active' : ''" class="
                        btn btn-xs btn-danger btn-default">OFF</a>
                        </div>
                    </div>
                    <div class="col-md-2 tac">
                        <div class="btn-group" role="group">
                            <a href="#" v-on:click="changeDefault('')"
                               v-bind:class="(params.filters.default === '') ? 'active' : ''" class="
                        btn btn-xs btn-default">ALL</a>
                            <a href="#" v-on:click="changeDefault(1)"
                               v-bind:class="(params.filters.default === 1) ? 'active' : ''" class="
                        btn btn-xs btn-success btn-default">ON</a>
                            <a href="#" v-on:click="changeDefault(0)"
                               v-bind:class="(params.filters.default === 0) ? 'active' : ''" class="
                        btn btn-xs btn-danger btn-default">OFF</a>
                        </div>
                    </div>

                    <div class="col-md-2"></div>

                </div>

                {{-- DATA --}}

                <div  v-for="item in data" class="list-group-item item">
                    <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.id }}</div>
                    <div class="col-md-3 dinamic"><span class="m-title">NAME: </span>@{{ item.name }}</div>
                    <div class="col-md-2 dinamic"><span class="m-title">CODE: </span>@{{ item.code }}</div>
                    <div class="col-md-2 tac dinamic">
                        <span class="m-title">STATUS: </span>
                        <span v-if="item.active" class="label label-success">ON</span>
                        <span v-if="item.active == 0 || item.active == null" class="label label-danger">OFF</span>
                    </div>
                    <div class="col-md-2 tac dinamic">
                        <span class="m-title">DEFAULT: </span>
                        <span v-if="item.default" class="label label-success">ON</span>
                        <span v-if="item.default == 0 || item.default ==  null" class="label label-danger">OFF</span>
                    </div>

                    <div class="col-md-2 tar item-controls">
                        <a v-bind:href="'/api/languages/' + item.id + '/edit'" type="button" class="btn btn-sm btn-primary">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a v-on:click="removeConfirm(item)" type="button" class="btn btn-sm btn-danger">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                {{--FOOTER--}}
                <div class="list-group-item list-group-item-primary footer">
                    <div class="col-xs-12 col-md-4">

                        <div class="btn-group">
                            <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''" v-on:click="
                        changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
                        </div>


                    </div>
                    <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
                        <div v-if="page.pages.length > 1" class="btn-group">
                            <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages" v-on:click="
                        changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </view-languages>
@endsection