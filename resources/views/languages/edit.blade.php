@extends('admin.master')

@section('content')

    <view-edit-languages :language="{{ $language->ToJson() }}" inline-template>
        <div class="col-xs-12">

            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="language.id > 0">EDIT LANGUAGE</span>
                    <span v-if="language.id == -1">NEW LANGUAGE</span>
                    <strong>@{{ language.name }}</strong></legend>

                <div class="form-group col-md-6 h-100p" v-bind:class="fields.name.class">
                    <label for="">Name</label>
                    <input type="text" class="form-control" v-model="language.name" placeholder="ex:. FRENCH">
                    <span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
                </div>
                <div class="form-group col-md-6 h-100p" v-bind:class="fields.code.class">
                    <label for="">Code</label>
                    <input type="text" class="form-control" v-model="language.code" placeholder="ex:. FR">
                    <span class="help-block" v-show="!fields.code.state" v-text="fields.code.msg"></span>
                </div>

                <div class="form-group col-md-6 h-100p active-options">
                    <label for="" class="w100p">Active</label>
                    <div>
                        <input v-on:click="language.active = 1" type="checkbox"
                               :checked="language.active === 1">
                        <label for="checkbox1">YES&nbsp;&nbsp;&nbsp;&nbsp;

                        </label>

                        <input class="checkbox-danger" :checked="language.active === 0" type="checkbox" v-on:click="language.active = 0">
                        <label for="checkbox1">NO

                        </label>
                    </div>
                </div>
                <div class="form-group col-md-6 h-100p active-options">
                    <label for="" class="w100p">Default</label>
                    <div>
                        <input v-on:click="language.default = 1" type="checkbox"
                               :checked="language.default === 1">
                        <label for="checkbox1">YES&nbsp;&nbsp;&nbsp;&nbsp;
                        </label>
                        <input class="checkbox-danger" :checked="language.default === 0" type="checkbox" v-on:click="language.default = 0">
                        <label for="checkbox1">NO

                        </label>
                    </div>
                </div>
                <div class="form-controls tar">
                    <a  href="/api/languages" class="btn btn-secondary">CLOSE</a>
                    <button type="submit" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
    </view-edit-languages>
@endsection