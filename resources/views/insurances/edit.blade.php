@extends('admin.master')

@section('content')

    <view-edit-insurances :insurance="{{ $insurance->ToJson() }}" inline-template>
        <div class="col-xs-12">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="insurance.id > 0">EDIT TYPE</span>
                    <span v-if="insurance.id == -1">NEW TYPE</span>
                    <strong>@{{ insurance.name }}</strong></legend>

                <div class="form-group col-md-6 h-90" v-bind:class="fields.name.class">
                    <label for="">Name</label>
                    <input type="text" class="form-control" v-model="insurance.name" placeholder="ex:. Helsana">
                    <span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
                </div>
                <div class="form-group col-md-6 h-90">
                    <label for="">Type</label>
                    <select class="form-control" v-model="insurance.insurance_type_id">
                        @foreach($insurance_types as $insurance_type)
                            <option value="{{ $insurance_type->id }}"> {{ $insurance_type->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3 h-90" v-bind:class="fields.reference.class">
                    <label for="">Reference</label>

                    <input type="text" class="form-control" v-model="insurance.reference" placeholder="ex:. helsana">
                    <span class="help-block" v-show="!fields.name.state" v-text="fields.reference.msg"></span>
                </div>
                <div class="form-group col-md-3 h-90">
                    <label for="">Color</label>
                    <div id="ColorPicker" class="input-group colorpicker-component">
                        <input type="text"  v-model="insurance.color" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>
                </div>
                <div class="form-group col-md-3 h-90" >
                    <label for="">Urgence Number</label>
                    <input type="text" class="form-control" v-model="insurance.urgence_number" placeholder="Urgence number">
                </div>
                <div class="form-group col-md-3 h-90" >
                    <label for="">Urgence Number Foreign.</label>
                    <input type="text" class="form-control" v-model="insurance.urgence_number_ext" placeholder="Urgence number Foreign">
                </div>
                <div class="form-group col-xs-12 tar">
                    <a href="/api/insurances" class="btn btn-secondary">CLOSE</a>
                    <button type="submit" class="btn btn-primary">SAVE</button>
                </div>
            </form>
        </div>
    </view-edit-insurances>
@endsection