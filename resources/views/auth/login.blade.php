@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2>{{ $utils->translate('login') }}</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / <a
                                    href="/login">{{ $utils->translate('login') }}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <div class="container">
        <div class="row">
            <auth-login inline-template>
                <div v-bind:class="(ready) ? '' : 'hidden'"  class="col-md-4 col-md-offset-4 hidden">
                    <div class="alert alert-danger" v-show="fields.email.state == false"
                        v-text="fields.email.msg" ></div>
                    <div class="alert alert-danger"  v-show="fields.password.state == false"
                        v-text="fields.password.msg"></div>
                    <div class="alert alert-warning" v-show="result == false"
                    >{{ $utils->translate('bad-credentials') }}</div>


                    <div class="login-register-form">
                        <form v-on:submit="Submit">

                            <div class="form-group">
                                <input type="email"  v-model="model.email" class="form-control" placeholder="Email">
                                <span class="input-icon"><i class=" fa fa-envelope"></i></span>
                            </div>
                            <div class="form-group">
                                <input type="password" v-model="model.password" class="form-control " placeholder="Username">
                                <span class="input-icon"><i class=" fa fa-lock"></i></span>
                            </div>

                            <button type="submit" class="btn btn-lg btn-block btn-skin">
                                <i id="btn_loader" v-bind:class="(loading) ? '' : 'hidden'"
                                   class="fa fa-gear faa-spin animated hidden"></i>
                                {{ $utils->translate('login') }}
                            </button>
                        </form>
                    </div>
                </div>
            </auth-login>
        </div>
    </div>
    <div class="space120"></div>



@endsection
