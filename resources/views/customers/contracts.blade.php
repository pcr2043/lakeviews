@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2>
                        <a href="/dashboard"><i class="fa fa-chevron-circle-left white-color"
                                                aria-hidden="true"></i></a>
                        &nbsp;{{ $utils->translate('my-contracts') }}
                    </h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / <a href="/dashboard">DASHBOARD</a>
                            / <a href="/contracts">{{ $utils->translate('my-contracts')  }}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <customers-contracts name="{{ $user->name }}" from="{{ $user->email }}" to="info@lakeviews.ch" inline-template>

        <div class="container contracts">

            <div v-bind:class="(complete) ? '' : 'hidden'" class="alert alert-success">
                <i class="fa fa-check" aria-hidden="true"></i>
                {{ $utils->translate("message-send-success") }}
            </div>


            <div class="header">
                <select class="fl" v-on:change="bind()" v-model="year">
                    <option v-for="year in years" v-bind:value="year">@{{ year }}</option>
                </select>

                <select class="fr" v-model="payment">
                    <option value="0">ALL</option>
                    <option value="1">{{ $utils->translate('monthly') }}</option>
                    <option value="2">{{ $utils->translate('quarterly') }}</option>
                    <option value="3">{{ $utils->translate('half-year') }}</option>
                    <option value="4">{{ $utils->translate('annual') }}</option>
                </select>
            </div>

            <div class="panel panel-primary panel-type" v-for="type in data">
                <div class="panel-heading">
                    <span class="title">@{{ type.translation }}</span>

                    <i class="fa fa-chevron-circle-down expander" v-on:click="expand($event)"
                       aria-hidden="true"></i>

                    <span class="amount" v-if="payment=='0'">@{{ parseFloat(type.all).toFixed(2) }} CHF</span>
                    <span class="amount" v-if="payment=='1'">@{{ parseFloat(type.monthly).toFixed(2) }}
                        CHF</span>
                    <span class="amount" v-if="payment=='2'">@{{ parseFloat(type.quarterly).toFixed(2) }}
                        CHF</span>
                    <span class="amount" v-if="payment=='3'">@{{ parseFloat(type.halfyear).toFixed(2) }}
                        CHF</span>
                    <span class="amount" v-if="payment=='4'">@{{ parseFloat(type.annual).toFixed(2) }}
                        CHF</span>

                </div>
                <div class="panel-body" style="display: none">
                    <div class="panel panel-primary panel-contract" v-for="contract in type.contracts">
                        <div class="panel-heading">
                            <div class="col-md-3 title">
                                @{{ contract.name }}
                            </div>
                            <div class="col-md-5 description">
                                @{{ contract.description }}
                            </div>
                            <div class="col-md-4 controls">
                                <i class="fa fa-chevron-circle-down expander"
                                   v-on:click="expand($event)"
                                   aria-hidden="true"></i>
                                <span class="amount">@{{ parseFloat(contract.amount).toFixed(2) }}
                                    CHF</span>
                            </div>
                        </div>

                        <div class="panel-body" style="display: none">
                            <div class="block">
                                <div class="col-md-2 bold">{{ $utils->translate('name') }}</div>
                                <div class="col-md-4">@{{ contract.name }}</div>
                                <div class="col-md-2 bold">{{ $utils->translate('birth-date') }}</div>
                                <div class="col-md-4"> @{{ contract.date_birth }}</div>
                                <div class="col-md-2 bold">
                                    {{ $utils->translate('address') }}
                                </div>
                                <div class="col-md-10">
                                    @{{ contract.city }}
                                    , @{{ contract.address }}
                                    , @{{ contract.npa }} - @{{ contract.country }}
                                </div>
                            </div>
                            <div class="block">

                                <div class="col-md-2 bold">
                                    {{ $utils->translate('type-of-contract') }}
                                </div>
                                <div class="col-md-4 bold">
                                    @{{ contract.insurance.type }}
                                </div>

                                <div class="col-md-2 bold">
                                    {{ $utils->translate('company') }}
                                </div>
                                <div class="col-md-4 bold">
                                    @{{ contract.insurance.name }}
                                </div>

                                <div class="col-md-2 bold">
                                    {{ $utils->translate('apolice-number') }}
                                </div>
                                <div class="col-md-4">
                                    @{{ contract.apolice_number }}
                                </div>


                                <div class="col-md-6 bold">
                                    <span v-if="contract.managed == 1">{{ $utils->translate('managed') }}</span>
                                    <span v-if="contract.managed == 0">{{ $utils->translate('non-managed') }}</span>
                                </div>


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('date-start') }}
                                </div>
                                <div class="col-md-4">
                                    @{{ contract.date_start }}
                                </div>


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('date-end') }}
                                </div>
                                <div class="col-md-4">
                                    @{{ contract.date_end }}
                                </div>


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('prime') }}
                                </div>
                                <div class="col-md-4">
                                    @{{ contract.amount }}
                                </div>


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('periodicity') }}
                                </div>
                                <div class="col-md-4">
                                    <span v-if="contract.payment_type==='1'">{{ $utils->translate('monthly') }}</span>
                                    <span v-if="contract.payment_type==='2'">{{ $utils->translate('quarterly') }}</span>
                                    <span v-if="contract.payment_type==='3'">{{ $utils->translate('half-year') }}</span>
                                    <span v-if="contract.payment_type==='4'">{{ $utils->translate('annual') }}</span>
                                </div>


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('phone') }}
                                </div>
                                <div class="col-md-4">
                                    @{{ contract.urgence_number }}
                                </div>


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('urgence-number') }}
                                </div>
                                <div class="col-md-4">
                                    @{{ contract.insurance.urgence_number_ext }}
                                </div>

                            </div>
                            <div class="block">


                                <div class="col-md-2 bold">
                                    {{ $utils->translate('description') }}
                                </div>
                                <div class="col-md-10" v-html="contract.long_description">

                                </div>
                            </div>

                            <div class="block">
                                <div class="col-md-12 bold">
                                    {{ $utils->translate('documents') }}:
                                </div>

                                <div class="col-md-6 download" v-for="file in contract.files">
                                    <span class="name">@{{ file.name }}</span>
                                    <a v-bind:href="'/api/files/download/' + file.id"><i
                                                class="fa fa-cloud-download"
                                                aria-hidden="true"></i>
                                    </a>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="col-md-6 cell-label">
                        Total {{ $utils->translate('annual').' '.$utils->translate('monthly') }}</div>
                    <div class="col-md-6 cell-value">@{{ parseFloat(total_monthly * 12).toFixed(2) }} CHF</div>

                    <div class="col-md-6 cell-label">
                        Total {{ $utils->translate('annual').' '.$utils->translate('quarterly') }}</div>
                    <div class="col-md-6 cell-value">@{{ parseFloat(total_quarterly * 4).toFixed(2) }} CHF</div>

                    <div class="col-md-6 cell-label">
                        Total {{ $utils->translate('annual').' '.$utils->translate('half-year') }}</div>
                    <div class="col-md-6  cell-value">@{{ parseFloat(total_half_year * 2).toFixed(2) }} CHF</div>

                    <div class="col-md-6 cell-label">Total {{ $utils->translate('annual') }}</div>
                    <div class="col-md-6 cell-value">@{{ parseFloat(total_annual).toFixed(2) }} CHF</div>

                    <div class="col-md-6 cell-label bold">Total</div>
                    <div class="col-md-6 cell-value bold total-final">@{{parseFloat((total_monthly * 12) + (total_quarterly * 4)+  (total_half_year *2) +total_annual).toFixed(2) }}
                        CHF
                    </div>
                </div>
            </div>

            <div class="space70 tac">
                <button class="btn btn-primary btn-lg btn-animated" v-on:click="enable = !enable">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    {{ $utils->translate("send-message") }}
                </button>
            </div>


            <div class="col-md-12 hidden" v-bind:class="(enable) ? '': 'hidden'">
                <form v-on:submit="send">
                    <div class="form-group dinamic-50 first">
                        <label for="">{{ $utils->translate("from") }}</label>

                        <input type="text" class="form-control" name="name" id="name"
                               placeholder="{{ $user->email }}" v-model="from">
                        <span style="top:27px" class="input-icon"><i class=" fa fa-envelope"></i></span>

                    </div>
                    <div class="form-group dinamic-50">
                        <label for="">{{ $utils->translate("to") }}</label>

                        <input type="text" class="form-control" name="to" id="to"
                               placeholder="{{ $user->relocation_email }}" v-model="to">
                        <span style="top:27px" class="input-icon"><i class=" fa fa-envelope"></i></span>

                    </div>


                    <div class="form-group">
                        <label for="">{{ $utils->translate("message") }}</label>

                        <ckeditor id="message" name="message"
                                  :field.sync="message"
                                  toolbar="basic"
                                  heigth="200"
                        ></ckeditor>
                    </div>

                    <div class="form-group form-footer tar">
                        <button class="btn btn-lg btn-block btn-skin">
                            <i class="fa fa-gear faa-spin animated hidden" v-bind:class="(loading ? '' : 'hidden')"
                               aria-hidden="true"></i>
                            {{ $utils->translate("send") }}
                        </button>
                    </div>

                </form>

            </div>

        </div>
    </customers-contracts>
    <div class="space70"></div>
@endsection
