@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2><a href="/dashboard"><i class="fa fa-chevron-circle-left white-color"
                                                aria-hidden="true"></i></a>
                        &nbsp; {{ $utils->translate('my-documents') }}</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / <a href="/dashboard">DASHBOARD</a>
                            / <a href="/documents">{{ $utils->translate('my-documents')  }}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <customers-documents inline-template>
        <div class="container">

            <div v-bind:class="(complete) ? '' : 'hidden'" class="alert alert-success hidden">
                <i class="fa fa-check" aria-hidden="true"></i>
                {{ $utils->translate("message-upload-success") }}
            </div>


            <div class="alert alert-danger hidden" v-bind:class="(confirm) ? '' : 'hidden'">

                    <span v-if="deleting==false">
                         <i class="fa fa-exclamation" aria-hidden="true"></i>
                        {{ $utils->translate("are-you-sure-about-to-delete-this-file") }}</span>
                <span v-if="deleting==true">
                          <i class="fa fa-gear faa-spin animated"></i>
                    {{ $utils->translate('deleting-file') }}</span>
                <button v-on:click="deleteExecute()" class="btn btn-primary fr">
                    {{ $utils->translate("yes") }}
                </button>
                <button v-on:click="confirm = false" style="margin-right: 10px; background-color: #ba3b3b" class="btn btn-primary cancel fr">
                    {{ $utils->translate("no") }}
                </button>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <strong>{{ $utils->translate('upload-a-document') }}</strong>
                    <input type="file" name="uploads" id="uploads">
                </div>
                <div class="panel-body" v-if="upload != null">
                    <div class="col-md-2 cell-label">{{  $utils->translate('name') }}</div>
                    <div class="col-md-10 cell-value">@{{ upload.name  }}</div>

                    <div class="col-md-2 cell-label">{{  $utils->translate('description') }}</div>
                    <div class="col-md-10 cell-value">
                        <ckeditor id="description" name="description"
                                  :field.sync="upload.description"
                                  toolbar="basic"
                                  heigth="200"
                        ></ckeditor>
                    </div>
                    <div class="col-md-offset-2 col-md-10">
                        <button v-on:click="uploadFile" class="btn btn-lg btn-block btn-skin">
                            <i class="fa fa-gear faa-spin animated hidden" v-bind:class="(loading ? '' : 'hidden')"
                               aria-hidden="true"></i>
                            {{ $utils->translate("upload") }}
                        </button>
                    </div>
                </div>


            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    {{ $utils->translate("documents") }}
                </div>

                <div class="panel-body">

                    <div class="col-md-12 download file" style="width: calc(100% - 20px)" v-for="file in files">
                        <span class="name">@{{ file.name }}</span>
                        <a v-bind:href="'/api/files/download/' + file.id"><i
                                    class="fa fa-cloud-download"
                                    aria-hidden="true"></i>
                        </a>
                        <a class="remove" v-on:click="deleteConfirm(file)">
                            <i class="fa fa-trash delete" aria-hidden="true"></i>
                        </a>
                        <div class="col-xs-12 no-padding">
                            @{{ file.description }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </customers-documents>
    <div class="space70"></div>


@endsection
