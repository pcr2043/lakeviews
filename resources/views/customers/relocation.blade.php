@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2><a href="/dashboard"><i class="fa fa-chevron-circle-left white-color" aria-hidden="true"></i></a>
                        &nbsp;{{ $utils->translate('relocation-services') }}</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / <a href="/dashboard">DASHBOARD</a>
                            / <a href="/relocation">{{ $utils->translate('relocation-services')  }}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <customers-relocation name="{{ $user->name }}" from="{{ $user->email }}" to="{{$user->relocation_email}}"
                          inline-template>
        <div class="container">

            <div v-bind:class="(complete) ? '' : 'hidden'" class="alert alert-success">
                <i class="fa fa-check" aria-hidden="true"></i>
                {{ $utils->translate("message-send-success") }}
            </div>


            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $utils->translate("relocation-info") }}</h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-3 bold panel-item">{{ $utils->translate("relocation-company") }}</div>
                    <div class="col-md-9 panel-item-2">{{ $user->relocation_company }}</div>

                    <div class="col-md-3 bold panel-item">{{ $utils->translate("email") }}</div>
                    <div class="col-md-9 panel-item-2">{{ $user->relocation_email }}</div>

                    <div class="col-md-3 bold panel-item">{{ $utils->translate("address") }}</div>
                    <div class="col-md-9 panel-item-2">{{ $user->relocation_address }}</div>

                    <div class="col-md-3 bold panel-item">{{ $utils->translate("description") }}</div>
                    <div class="col-md-9 panel-item-2">{!!   $user->relocation_text !!}</div>

                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{  $utils->translate("documents")}}</h3>
                </div>
                <div class="panel-body">
                    @foreach($user->RelocationFiles() as $document)
                        <div class="col-md-12 download">
                            {{ $document->name }}
                            <a href="{{ '/api/files/download/'.$document->id }}"><i
                                        class="fa fa-cloud-download" aria-hidden="true"></i></a>
                        </div>
                    @endforeach
                </div>
            </div>


            <div class="space70 tac">
                <button class="btn btn-primary btn-lg btn-animated" v-on:click="enable = !enable">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    {{ $utils->translate("send-message") }}
                </button>
            </div>



            <div class="col-md-12 hidden"  v-bind:class="(enable) ? '': 'hidden'" >
                <form v-on:submit="send">
                    <div class="form-group dinamic-50 first">
                        <label for="">{{ $utils->translate("from") }}</label>

                        <input type="text" class="form-control" name="name" id="name"
                               placeholder="{{ $user->email }}" v-model="from">
                        <span style="top:27px" class="input-icon"><i class=" fa fa-envelope"></i></span>

                    </div>
                    <div class="form-group dinamic-50">
                        <label for="">{{ $utils->translate("to") }}</label>

                        <input type="text" class="form-control" name="to" id="to"
                               placeholder="{{ $user->relocation_email }}" v-model="to">
                        <span style="top:27px" class="input-icon"><i class=" fa fa-envelope"></i></span>

                    </div>


                    <div class="form-group">
                        <label for="">{{ $utils->translate("message") }}</label>

                        <ckeditor id="message" name="message"
                                  :field.sync="message"
                                  toolbar="basic"
                                  heigth="200"
                        ></ckeditor>
                    </div>

                    <div class="form-group form-footer tar">
                        <button class="btn btn-lg btn-block btn-skin">
                            <i class="fa fa-gear faa-spin animated hidden" v-bind:class="(loading ? '' : 'hidden')"
                               aria-hidden="true"></i>
                            {{ $utils->translate("send") }}
                        </button>
                    </div>

                </form>

            </div>

        </div>
    </customers-relocation>
    <div class="space70"></div>
@endsection
