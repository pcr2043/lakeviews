@extends('master-nav')

@section('content')
    <div class="page-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2>DASHBOARD</h2>
                </div>
                <div class="col-sm-6 text-right">
                    <ol class="breadcrumb">
                        <li>
                            <a href="/">{{ $utils->translate('home') }}</a> / <a href="/dashboard">DASHBOARD</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="space70"></div>
    <div class="container">
        <div class="row">
            <customers-index inline-template>

                @if(Auth::User()->relocation)
                    <div class="col-md-3 col-sm-6 margin-btm-30">
                        <div class="green-boxe" v-on:click="getUrl('relocation')">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <h3>{{ $utils->translate('relocation-services') }}</h3>
                            <p>
                                {{ $utils->translate('relocation-services-details') }}
                            </p>
                            <div class="details">{{ $utils->translate('more-details') }}<i
                                        class="fa fa-caret-right"></i>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col-md-3 col-sm-6 margin-btm-30">
                    <div class="green-boxe" v-on:click="getUrl('contracts')">
                        <i class="fa fa-umbrella" aria-hidden="true"></i>
                        <h3>{{ $utils->translate('my-contracts') }}</h3>
                        <p>
                            {{ $utils->translate('my-contracts-details') }}
                        </p>
                        <div class="details">{{ $utils->translate('more-details') }}<i
                                    class="fa fa-caret-right"></i>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 margin-btm-30">
                    <div class="green-boxe" v-on:click="getUrl('documents')">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                        <h3>{{ $utils->translate('my-documents') }}</h3>
                        <p>
                            {{ $utils->translate('my-documents-details') }}
                        </p>
                        <div class="details">{{ $utils->translate('more-details') }}<i
                                    class="fa fa-caret-right"></i>
                        </div>
                    </div>
                </div>
            </customers-index>
        </div>
    </div>
    <div class="space120"></div>



@endsection
