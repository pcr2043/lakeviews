@extends('admin.master')

@section('content')

    <view-edit-contracts :contract="{{ $contract->ToJson() }}" inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <form v-on:submit="save" method="POST" role="form">

                <legend>
                    <span v-if="contract.id > 0">EDIT CONTRACT</span>
                    <span v-if="contract.id == -1">NEW CONTRACT</span>
                    <strong>@{{ contract.apolice_number }}</strong></legend>

                <div class="form-group col-md-5 h-90">
                    <label for="" class="wp100">Customer</label>
                    <select id="customers" class="form-control" v-model="contract.customer_id">
                        @foreach($customers as $customer)
                            <option data-customer="{{ $customer->ToJson() }}" value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endforeach
                    </select>

                </div>
                <div class="form-group col-md-1 h-90">
                    <button type="button" class="btn btn-primary" v-on:click="copy()" style="position: relative; top:25px">COPY</button>
                </div>
                <div class="form-group col-md-6 h-90">
                    <label for="" class="wp100">Insurance</label>
                    <select id="insurances" class="form-control" v-model="contract.insurance_id">
                        @foreach($insurances as $insurance)
                            <option value="{{ $insurance->id }}">{{ $insurance->InsuranceType->name.' | '.$insurance->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3 h-90" v-bind:class="fields.apolice_number.class">
                    <label for="">Apolice Number</label>
                    <input type="text" class="form-control" v-model="contract.apolice_number" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.apolice_number.state"
                          v-text="fields.apolice_number.msg"></span>
                </div>
                <div class="form-group col-md-3 h-90" v-bind:class="fields.apolice_year.class">
                    <label for="">Apolice Year</label>
                    <input type="text" class="form-control" v-model="contract.apolice_year" placeholder="ex:. {{  \Carbon\Carbon::now()->year }}">
                    <span class="help-block" v-show="!fields.apolice_year.state"
                          v-text="fields.apolice_year.msg"></span>
                </div>

                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">Closed</label>
                    <div>
                        <input v-on:click="contract.closed = 1" type="checkbox"
                               :checked="contract.closed === 1">
                        <label for="checkbox1">YES </label>&nbsp;
                        <input class="checkbox-danger" :checked="contract.closed === 0" type="checkbox"
                               v-on:click="contract.closed = 0">
                        <label for="checkbox1">NO</label>
                    </div>
                </div>
                <div class="form-group col-md-3 h-90">
                    <label for="" class="w100p">Managed</label>
                    <div>
                        <input v-on:click="contract.managed = 1" type="checkbox"
                               :checked="contract.managed === 1">
                        <label for="checkbox1">YES </label>&nbsp;
                        <input class="checkbox-danger" :checked="contract.managed === 0" type="checkbox"
                               v-on:click="contract.managed = 0">
                        <label for="checkbox1">NO</label>
                    </div>
                </div>
                <div class="form-group col-md-4 h-90" v-bind:class="fields.first_name.class">
                    <label for="">First Name</label>
                    <input type="text" class="form-control" v-model="contract.first_name" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.first_name.state"
                          v-text="fields.first_name.msg"></span>
                </div>

                <div class="form-group col-md-4 h-90" v-bind:class="fields.last_name.class">
                    <label for="">Last Name</label>
                    <input  type="text" class="form-control" v-model="contract.last_name" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.last_name.state"
                          v-text="fields.last_name.msg"></span>
                </div>

                <div class="form-group col-md-4 h-90">
                    <label for="">Email</label>
                    <input type="email" class="form-control" v-model="contract.email" placeholder="ex:. home">
                </div>

                <div class="form-group col-md-3 h-90">
                    <label for="">Genre</label>
                    <div>
                        <input v-on:click="contract.gender = 1" type="checkbox"
                               :checked="contract.gender === 1">
                        <label for="checkbox1">MALE </label>&nbsp;
                        <input class="checkbox-danger" :checked="contract.gender === 0" type="checkbox"
                               v-on:click="contract.gender = 0">
                        <label for="checkbox1">FEMALE</label>
                    </div>
                </div>

                <div class="form-group col-md-3 h-90" v-bind:class="fields.date_birth.class">
                    <label for="">Date of Birth</label>
                    <input type="date" class="form-control" v-model="contract.date_birth" placeholder="ex:. home">
                    <span class="help-block" v-show="!fields.date_birth.state"
                          v-text="fields.date_birth.msg"></span>
                </div>


                <div class="form-group col-md-6 h-90">
                    <label for="">Country</label>
                    <select id="country" name="country" v-model="contract.country" class="form-control"
                            data-country-value="{!! $contract->country !!}">
                        <option value="AF">Afghanistan</option>
                        <option value="AX">Åland Islands</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AQ">Antarctica</option>
                        <option value="AG">Antigua and Barbuda</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia, Plurinational State of</option>
                        <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                        <option value="BA">Bosnia and Herzegovina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, the Democratic Republic of the</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Côte d'Ivoire</option>
                        <option value="HR">Croatia</option>
                        <option value="CU">Cuba</option>
                        <option value="CW">Curaçao</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GG">Guernsey</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard Island and McDonald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran, Islamic Republic of</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IM">Isle of Man</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JE">Jersey</option>
                        <option value="JO">Jordan</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="KE">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic of</option>
                        <option value="KR">Korea, Republic of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="MO">Macao</option>
                        <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States of</option>
                        <option value="MD">Moldova, Republic of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="ME">Montenegro</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="OM">Oman</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PS">Palestinian Territory, Occupied</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                        <option value="QA">Qatar</option>
                        <option value="RE">Réunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="BL">Saint Barthélemy</option>
                        <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                        <option value="KN">Saint Kitts and Nevis</option>
                        <option value="LC">Saint Lucia</option>
                        <option value="MF">Saint Martin (French part)</option>
                        <option value="PM">Saint Pierre and Miquelon</option>
                        <option value="VC">Saint Vincent and the Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome and Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="RS">Serbia</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SX">Sint Maarten (Dutch part)</option>
                        <option value="SK">Slovakia</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                        <option value="SS">South Sudan</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard and Jan Mayen</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH" selected>Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="TW">Taiwan, Province of China</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic of</option>
                        <option value="TH">Thailand</option>
                        <option value="TL">Timor-Leste</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad and Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks and Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela, Bolivarian Republic of</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands, British</option>
                        <option value="VI">Virgin Islands, U.S.</option>
                        <option value="WF">Wallis and Futuna</option>
                        <option value="EH">Western Sahara</option>
                        <option value="YE">Yemen</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
                    </select>
                </div>

                <div class="form-group col-md-7 h-90" v-bind:class="fields.address.class">
                    <label for="">Address</label>
                    <input type="text" class="form-control" v-model="contract.address"
                           placeholder="ex:. Rue de La Praille">
                    <span class="help-block" v-show="!fields.address.state"
                          v-text="fields.address.msg"></span>
                </div>
                <div class="form-group col-md-3 h-90" v-bind:class="fields.city.class">
                    <label for="">City</label>
                    <input type="text" class="form-control" v-model="contract.city" placeholder="ex:. Genéve">
                    <span class="help-block" v-show="!fields.city.state"
                          v-text="fields.city.msg"></span>
                </div>
                <div class="form-group col-md-2 h-90" v-bind:class="fields.npa.class">
                    <label for="">NPA</label>
                    <input type="text" class="form-control" v-model="contract.npa" placeholder="ex:. 1200">
                    <span class="help-block" v-show="!fields.npa.state"
                          v-text="fields.npa.msg"></span>
                </div>

                <div class="form-group col-md-6 h-90">
                    <label class="wp100" for="">Payment Type</label>
                    <select class="form-control" v-model="contract.payment_type">
                        <option value="1">Monthly</option>
                        <option value="2">Quarterly</option>
                        <option value="3">Half-Year</option>
                        <option value="3">Annual</option>
                    </select>
                </div>

                <div class="form-group col-md-6 h-90">
                    <label for="">Amount</label>
                    <input type="text" class="form-control" v-model="contract.amount" placeholder="ex:. 2016-12-10">

                </div>

                <div class="form-group col-md-2 h-90" v-bind:class="fields.date_start.class">
                    <label for="">Date Start</label>
                    <input type="date" class="form-control" v-model="contract.date_start" placeholder="ex:. 2016-12-10">
                    <span class="help-block" v-show="!fields.date_start.state"
                          v-text="fields.date_start.msg"></span>
                </div>

                <div class="form-group col-md-2 h-90" v-bind:class="fields.date_end.class">
                    <label for="">Date End</label>
                    <input type="date" class="form-control" v-model="contract.date_end" placeholder="ex:. 2017-12-10">
                    <span class="help-block" v-show="!fields.date_end.state"
                          v-text="fields.date_end.msg"></span>
                </div>

                <div class="form-group col-md-3 h-90">
                    <label for="">Notifications Days</label>
                    <div id="alert_interval_days"></div>
                    <div id="alert_interval_days_labels" class="slider-labels"></div>
                </div>
                <div class="form-group col-md-1 h-90">
                </div>
                <div class="form-group col-md-3 h-90">
                    <label for="">Marketing Period</label>
                    <div id="renew_interval_days"></div>
                    <div id="renew_interval_days_labels" class="slider-labels"></div>
                </div>

                <div class="form-group col-md-12  uploads">
                    <label for="">Uploads</label>
                    <input type="file" name="uploads" id="uploads" multiple="multiple">
                    <div class="list-group">
                        <div v-for="file in files" class="list-group-item list-group-item-download">
                            <i v-on:click="removeStorage(file)" class="fa fa-trash remove" aria-hidden="true"></i>
                            @{{ file.name }}
                            <a v-bind:href="'/api/files/download/' + file.id"><i class="fa fa-cloud-download download"
                                                                                 aria-hidden="true"></i></a>
                        </div>
                        <div v-for="file in uploads" class="list-group-item list-group-item-download">
                            <i v-on:click="remove(file)" class="fa fa-trash remove" aria-hidden="true"></i>
                            @{{ file.name }}
                            <a><i v-on:click="download(file)" class="fa fa-cloud-download download"
                                  aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>

                <div class="form-group col-md-12 ">
                    <label for="">Short Description</label>
                    <textarea rows="3" type="date" class="form-control" v-model="contract.description"
                              placeholder="ex:. Hello"></textarea>

                </div>

                <div class="form-group col-md-12">
                    <label class="wp100" for="">Long Description</label>
                    <ckeditor id="longDescription" name="page"
                              :field.sync="contract.long_description"
                              toolbar="default"
                              heigth="300"
                    ></ckeditor>
                </div>


                <div class="form-group col-xs-12 tar">
                    <a href="/api/contracts" class="btn btn-secondary">CLOSE</a>

                    <button type="submit" class="btn btn-primary btn-animated">
                        <i v-bind:class="(loading) ? 'visible' : 'hidden'" id="btn_loader"
                           class="fa fa-gear faa-spin animated hidden"></i>
                        SAVE
                    </button>
                </div>
            </form>
        </div>
    </view-edit-contracts>
@endsection