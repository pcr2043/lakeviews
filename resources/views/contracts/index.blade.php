@extends('admin.master')

@section('content')

    <view-contracts inline-template>
        <div class="col-xs-12 hidden" v-bind:class="(loaded) ? '' : 'hidden'">
            <div class="list-group list-table">
                {{-- HEADER TITLES --}}
                <div class="list-group-item header">
                    <div class="col-md-1">Apolice Number</div>
                    <div class="col-md-1">Apolice Year</div>
                    <div class="col-md-1">Date Start</div>
                    <div class="col-md-1">Date End</div>
                    <div class="col-md-2">Customer</div>
                    <div class="col-md-1">Name</div>
                    <div class="col-md-1">Insurance</div>
                    <div class="col-md-1">Status</div>
                    <div class="col-md-1">Managed</div>
                    <div class="col-md-1 tar">
                        <a href="/api/contracts/-1/edit" type="button" class="btn btn-sm btn-secondary"
                        ><i class="fa fa-plus" aria-hidden="true"></i> NEW</a>

                    </div>
                </div>

                <div class="list-group-item filters">
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.apolice_number" v-on:keyup="rebind()"
                               placeholder="type a Apolice">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.apolice_year" v-on:keyup="rebind()"
                               placeholder="Year">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.date_start" v-on:keyup="rebind()"
                               placeholder="Date">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.date_end" v-on:keyup="rebind()"
                               placeholder="Date">
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" v-model="params.filters.customer" v-on:keyup="rebind()"
                               placeholder="type a Name">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.name" v-on:keyup="rebind()"
                               placeholder="type a Name" >
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.company" v-on:keyup="rebind()"
                               placeholder="type a Company" >
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.type" v-on:keyup="rebind()"
                               placeholder="type a Type" >
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.status" v-on:keyup="rebind()"
                               placeholder="type a Status">
                    </div>
                    <div class="col-md-1">
                        <input type="text" class="form-control" v-model="params.filters.managed" v-on:keyup="rebind()"
                               placeholder="1 or 0">
                    </div>

                    <div class="col-md-1"></div>

                </div>

                {{-- DATA --}}

                <div v-for="item in data" class="list-group-item item">
                    <div class="col-md-1 dinamic"><span class="m-title">ID: </span>@{{ item.apolice_number }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">YEAR: </span>@{{ item.apolice_year }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">DATE START: </span>@{{ item.date_start }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">DATE END: </span>@{{ item.date_end }}</div>
                    <div class="col-md-2 dinamic"><span class="m-title">CUSTOMER: </span>@{{ item.customer.name }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">NAME: </span>@{{ item.name }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">COMPANY: </span>@{{ item.insurance.name }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">TYPE: </span>@{{ item.insurance.type }}</div>
                    <div class="col-md-1 dinamic"><span class="m-title">STATUS: </span>
                        <span v-if="item.status == 'ALERT'" class="label label-info">ALERT</span>
                        <span v-if="item.status == 'RENEW'" class="label label-warning">RENEW</span>
                        <span v-if="item.status == 'EXPIRED'" class="label label-danger">EXPERID</span>
                        <span v-if="item.status == 'RUNNING'" class="label label-success">RUNNING</span>
                    </div>
                    <div class="col-md-1 dinamic"><span class="m-title">STATUS: </span>
                        <span v-if="item.managed == true" class="label label-info">MANAGED</span>
                        <span v-if="item.managed == false" class="label label-primary">NOT MANAGED</span>
                    </div>
                    <div class="col-md-1 tar item-controls">
                        <a v-bind:href="'/api/contracts/' + item.id + '/edit'" type="button" class="btn btn-sm btn-primary">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a v-on:click="removeConfirm(item)" type="button" class="btn btn-sm btn-danger">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                {{--FOOTER--}}
                <div class="list-group-item list-group-item-primary footer">
                    <div class="col-xs-12 col-md-4">

                        <div class="btn-group">
                            <a v-for="size in page.sizes" v-bind:class="(size== params.take) ? 'active' : ''"
                               v-on:click="
                        changeSize(size)" href="#" class="btn btn-default btn-sm">@{{ size }}</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-offset-4 col-md-4 tar">
                        <div v-if="page.pages.length > 1" class="btn-group">
                            <a v-bind:class="(page == params.page) ? 'active' : ''" v-for="page in page.pages"
                               v-on:click="
                        changePage(page)" href="#" class="btn btn-sm btn-default">@{{ (page + 1) }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </view-contracts>
@endsection