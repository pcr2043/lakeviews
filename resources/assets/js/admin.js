/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*UTIL COMPONENTES*/
Vue.component('noty', require('./components/Noty.vue'));
Vue.component('ckeditor', require('./components/Ckeditor.vue'));

/*VIEWS*/

Vue.component('view-users', require('./views/users/index.vue'));
Vue.component('view-edit-users', require('./views/users/edit.vue'));

Vue.component('view-languages', require('./views/languages/index.vue'));
Vue.component('view-edit-languages', require('./views/languages/edit.vue'));

Vue.component('view-pages', require('./views/pages/index.vue'));
Vue.component('view-edit-pages', require('./views/pages/edit.vue'));

Vue.component('view-contracts', require('./views/contracts/index.vue'));
Vue.component('view-edit-contracts', require('./views/contracts/edit.vue'));

Vue.component('view-words', require('./views/words/index.vue'));
Vue.component('view-edit-words', require('./views/words/edit.vue'));

Vue.component('view-insurance-types', require('./views/insurance-types/index.vue'));
Vue.component('view-edit-insurance-types', require('./views/insurance-types/edit.vue'));

Vue.component('view-insurances', require('./views/insurances/index.vue'));
Vue.component('view-edit-insurances', require('./views/insurances/edit.vue'));

const app = new Vue({
    el: '#app',
    data: {
        notifications: []
    },

    methods: {
        setUpNotifications: function () {

            var local = this;
            local.notifications.forEach(function (noty, index) {

                if (noty.timeout != null) {
                    setTimeout(function () {

                        local.notifications.$remove(noty);
                    }, noty.timeout)
                }
            })
        },
    },
    events: {

        noty: function (noty) {
            console.log(noty);
            this.notifications.push(noty);
            this.setUpNotifications();
        },
        execute: function (fn, noty) {

            var fn_name = fn.substring(0, fn.indexOf('('));
            var params = fn.substring(fn.indexOf('(') + 1, fn.indexOf(')'));

            this.$broadcast(fn_name, params);
            if (noty != null)
                this.notifications.$remove(noty);

        }
    }
});
