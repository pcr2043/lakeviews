/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*COMPONENTS*/
Vue.component('ckeditor', require('./components/Ckeditor.vue'));

/*VIEWS*/
Vue.component('auth-login', require('./views/auth/login.vue'));
Vue.component('contacts-index', require('./views/contact/index.vue'))
/*CUSTOMERS*/
Vue.component('customers-index', require('./views/customers/index.vue'));
Vue.component('customers-relocation', require('./views/customers/relocation.vue'));
Vue.component('customers-contracts', require('./views/customers/contracts.vue'));
Vue.component('customers-documents', require('./views/customers/documents.vue'));

const app = new Vue({
    el: '#app',
    data: {
        loaded: false,
    },
    methods: {
        getUrl: function (url) {
            window.location.pathname = url;
        }
    },
    ready: function () {

        var local = this;
        setTimeout(function () {
            local.loaded = true;
        }, 500);


    }
});
